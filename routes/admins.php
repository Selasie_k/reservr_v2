<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admins\UserController;
use App\Http\Controllers\Admins\EventController;
use App\Http\Controllers\Admins\BusinessController;
use App\Http\Controllers\Admins\CategoryController;
use App\Http\Controllers\Admins\DashboardController;
use App\Http\Controllers\Admins\ReservationController;
use App\Http\Controllers\Admins\SubCategoryController;
use App\Http\Controllers\Admins\ActiveBusinessController;
use App\Http\Controllers\Admins\UserImpersonationController;

Route::get('/', [DashboardController::class, 'index'])
        ->name('.dashboard');

Route::group(['as' => '.users', 'prefix' => 'users'],function(){
        Route::get('/', [UserController::class, 'index']);
        Route::get('/{user}', [UserController::class, 'show'])->name('.show');
        Route::get('/{user}/impersonate', [UserImpersonationController::class, 'create'])->name('.impersonate');
});

Route::group(['as' => '.events', 'prefix' => 'events'],function(){
        Route::get('/', [EventController::class, 'index']);
        Route::get('/{event}', [EventController::class, 'show'])->name('.show');
});

Route::group(['as' => '.categories', 'prefix' => 'categories'],function(){
        Route::get('/', [CategoryController::class, 'index']);
        Route::get('/{category}', [CategoryController::class, 'show'])->name('.show');
        Route::post('/', [CategoryController::class, 'store'])->name('.store');
        Route::patch('/{category}', [CategoryController::class, 'update'])->name('.update');
        Route::delete('/{category}', [CategoryController::class, 'destroy'])->name('.destroy');
});

Route::group(['as' => '.sub-categories', 'prefix' => 'categories'],function(){
        Route::post('/{category}', [SubCategoryController::class, 'store'])->name('.store');
        Route::delete('/{subCategory}', [SubCategoryController::class, 'destroy'])->name('.destroy');
});

Route::group(['as' => '.businesses', 'prefix' => 'businesses'],function(){
        Route::get('/', [BusinessController::class, 'index']);
        Route::get('/create', [BusinessController::class, 'create'])->name('.create');
        Route::post('/store', [BusinessController::class, 'store'])->name('.store');
        Route::get('/{business}/edit', [BusinessController::class, 'edit'])->name('.edit');
        Route::post('/{business}/update', [BusinessController::class, 'update'])->name('.update');
        Route::patch('/{business}/feature', [BusinessController::class, 'feature'])->name('.feature');
        Route::delete('/{business}', [BusinessController::class, 'destroy'])->name('.destroy');
        Route::patch('/{business}/toggle-active', [ActiveBusinessController::class, 'toggle'])->name('.toggle-active');
});

Route::get('/reservations', [ReservationController::class, 'index'])
        ->name('.reservations');
