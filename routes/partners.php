<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Partners\PurchaseController;
use App\Http\Controllers\Partners\TeamController;
use App\Http\Controllers\Partners\EventController;
use App\Http\Controllers\Partners\ServiceController;
use App\Http\Controllers\Partners\BusinessController;
use App\Http\Controllers\Users\EventTicketController;
use App\Http\Controllers\Partners\DashboardController;
use App\Http\Controllers\Partners\Auth\LoginController;
use App\Http\Controllers\Partners\TeamInviteController;
use App\Http\Controllers\Partners\TeamMemberController;
use App\Http\Controllers\Partners\AvailabilityController;
use App\Http\Controllers\Partners\OnlineStatusController;
use App\Http\Controllers\Partners\ReservationsController;
use App\Http\Controllers\Partners\BusinessSetupController;
use App\Http\Controllers\Partners\PublishedEventController;
use App\Http\Controllers\Admins\UserImpersonationController;
use App\Http\Controllers\Partners\AcceptsBookingsController;
use App\Http\Controllers\Partners\Onboarding\DetailsController;
use App\Http\Controllers\Partners\ReservationCheckinController;
use App\Http\Controllers\Partners\Auth\BusinessOptionController;
use App\Http\Controllers\Partners\Onboarding\CreateAccountController;
use App\Http\Controllers\Partners\ReservationStatusContontroller;
use App\Http\Controllers\Partners\ReviewController;
use App\Http\Controllers\PhotoController;

Route::group(['prefix' => '/partners/onboarding'], function(){
        Route::get('/', [DetailsController::class, 'create'])->name('partners.onboarding.create');
        Route::post('/', [DetailsController::class, 'store'])->name('partners.onboarding.store');
        Route::get('/account/{user?}', [CreateAccountController::class, 'create'])->name('partners.onboarding.account.create');
        Route::post('/account', [CreateAccountController::class, 'store'])->name('partners.onboarding.account.store');
});
// Route::post('/partners/onboarding/create-account', [DetailsController::class, 'create'])->name('partners.onboarding.store');

Route::get('/partners/login', [LoginController::class, 'create'])->name('partners.login');

Route::get('/partners/login', [LoginController::class, 'create'])->name('partners.login');
Route::post('/partners/login', [LoginController::class, 'store'])->name('partners.login.store');

Route::group(['as' => 'partners.', 'prefix' => 'partners', 'middleware' => ['auth.partner']], function() {
    Route::get('/dashboard', DashboardController::class)->name('dashboard');
    Route::get('/auth/choose-business', BusinessOptionController::class)->name('auth.business-option');
    Route::get('/auth/{business}/select', [BusinessOptionController::class, 'select'])->name('auth.business-option.select');

    Route::group(['prefix' => 'businesses', 'as' => 'businesses'], function(){
        Route::get('/', [BusinessController::class, 'index']);
        Route::post('/', [BusinessController::class, 'store'])->name('.store');
        Route::get('/create', [BusinessController::class, 'create'])->name('.create');

        Route::group(['prefix' => '{business}'], function(){
                Route::get('/', [BusinessController::class, 'show'])->name('.show');
                Route::get('/edit', [BusinessController::class, 'edit'])->name('.edit');
                Route::post('/update', [BusinessController::class, 'update'])->name('.update');
                Route::get('/toggle-online', [OnlineStatusController::class, 'update'])->name('.online-status.toggle');
                Route::delete('/', [BusinessController::class, 'destroy'])->name('.destroy');
        });
    });

    Route::group(['prefix' => 'events', 'as' => 'events'], function(){
            Route::get('/', [EventController::class, 'index']);                 
            Route::get('/create', [EventController::class, 'create'])->name('.create');
            Route::get('/{event}', [EventController::class, 'show'])->name('.show');
            Route::post('/', [EventController::class, 'store'])->name('.store');
            Route::get('/{event}/edit', [EventController::class, 'edit'])->name('.edit');
            Route::post('/{event}/update', [EventController::class, 'update'])->name('.update');
            Route::patch('/{event}/publish', [PublishedEventController::class, 'store'])->name('.publish');
            Route::delete('/{event}/unpublish', [PublishedEventController::class, 'destroy'])->name('.unpublish');       
            Route::prefix('/tickets')->group(function(){
                    Route::get('/{ticket}', [EventTicketController::class, 'show'])->name('.tickets.show');
                    Route::post('/{event}', [EventTicketController::class, 'store'])->name('.tickets.store');
            });
    });

    Route::group(['prefix' => 'reservations', 'as' => 'reservations'], function(){
        Route::get('/', [ReservationsController::class, 'index']);
        Route::get('/create', [ReservationsController::class, 'create'])->name('.create');
        Route::get('/{reservation}/edit', [ReservationsController::class, 'edit'])->name('.edit');
        Route::get('/{reservation}', [ReservationsController::class, 'show'])->name('.show');
        Route::post('/', [ReservationsController::class, 'store'])->name('.store');
        Route::delete('/{reservation}', [ReservationsController::class, 'destroy'])->name('.destroy');
        Route::patch('/{reservation}/update-status', [ReservationStatusContontroller::class, 'update'])->name('.status.update');
        Route::patch('/{reservation}/check-in', [ReservationCheckinController::class, 'store'])->name('.check-in');

    });

    Route::group(['prefix' => 'business/profile', 'as' => 'profile'], function(){
            Route::get('/', [BusinessSetupController::class, 'show']);
            Route::post('/', [BusinessSetupController::class, 'update'])->name('.update');
            Route::patch('/toggle-online', [OnlineStatusController::class, 'update'])->name('.online-status.toggle');
            Route::patch('/accepting-bookings', [AcceptsBookingsController::class, 'update'])->name('.accepting-bookings.toggle');
            Route::patch('/details', [BusinessSetupController::class, 'updateDetails'])->name('.details.update');
            Route::patch('/location', [BusinessSetupController::class, 'updateLocation'])->name('.location.update');
            Route::patch('/social', [BusinessSetupController::class, 'updateSocialLinks'])->name('.social.update');
    });

    Route::group(['prefix' => 'availability', 'as' => 'availability'], function(){
        Route::get('/', [AvailabilityController::class, 'index']);
        Route::patch('/', [AvailabilityController::class, 'update'])->name('.update');
    });

    Route::group(['as' => 'reservations', 'prefix' => 'reservations'], function(){
        Route::get('/', [ReservationsController::class, 'index']);
        Route::post('/', [ReservationsController::class, 'store'])->name('.store');
        Route::get('/{reservation}', [ReservationsController::class, 'show'])->name('.show');
    });

    Route::group(['prefix' => 'services', 'as' => 'services'], function(){
        Route::get('/', [ServiceController::class, 'index']);
        Route::post('/store', [ServiceController::class, 'store'])->name('.store');
        Route::post('/update/{service}', [ServiceController::class, 'update'])->name('.update');
        Route::delete('/delete/{service}', [ServiceController::class, 'destroy'])->name('.destroy');
    });

    Route::group(['prefix' => 'photos', 'as' => 'photos'], function(){
        Route::get('/', [PhotoController::class, 'index']);
        Route::post('/store', [PhotoController::class, 'store'])->name('.store');
        Route::put('/make-banner', [PhotoController::class, 'banner'])->name('.make-banner');
        Route::delete('/{photo}/delete', [PhotoController::class, 'destroy'])->name('.destroy');
    });

    Route::group(['prefix' => 'reviews', 'as' => 'reviews'], function(){
        Route::get('/', [ReviewController::class, 'index']);
    });

    Route::group(['prefix' => 'teams', 'as' => 'teams'], function(){
            Route::get('/', [TeamController::class, 'index']);

            Route::get('/{team}', [TeamController::class, 'show'])
                    ->name('.show');

            Route::patch('/switch/{team}', [TeamController::class, 'switch'])
                    ->name('.switch');

            Route::delete('/leave/{team}', [TeamController::class, 'leave'])
                    ->name('.leave');

            Route::patch('/{team}/members/{user}/update', [TeamMemberController::class, 'update'])
                    ->name('.members.update');

            Route::delete('/{team}/members/remove/{user?}', [TeamMemberController::class, 'destroy'])
                    ->name('.members.remove');

            Route::post('/{team}/invite-member', [TeamInviteController::class, 'store'])
                    ->name('.invitations.store');

            Route::get('/invites/resend/{invite}', [TeamInviteController::class, 'resend'])
                    ->name('.invitations.resend');

            Route::get('/invites/accept/{token}', [TeamInviteController::class, 'accept'])
                    ->name('.invitations.accept');

            Route::delete('/invites/decline/{token}', [TeamInviteController::class, 'decline'])
                    ->name('.invitations.decline');

            Route::delete('/invites/{invite}/delete', [TeamInviteController::class, 'destroy'])
                    ->name('.invitations.delete');
    });

    Route::group(['prefix' => 'purchases', 'as' => 'purchases'], function(){
            Route::get('/', [PurchaseController::class, 'index']);
            Route::get('/purchase/{package:name}', [PurchaseController::class, 'store'])->name('.store');
    });

    
    Route::get('/impersonation/destroy', [UserImpersonationController::class, 'destroy'])
            ->name('impersonation.destroy');
});