<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\LegalController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\PaymentRedirectController;
use App\Http\Controllers\Users\ProfileController;
use App\Http\Controllers\EventDiscoveryController;
use App\Http\Controllers\Users\BookingsController;
use App\Http\Controllers\BusinessDiscoveryController;
use App\Http\Controllers\Users\EventTicketController;
use App\Http\Controllers\Users\ProfilePhotoController;
use App\Http\Controllers\GuestEventReservationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', FrontController::class)->name('home');

Route::get('/search', [SearchController::class, 'index'])
        ->name('search');

Route::get('/businesses', [BusinessDiscoveryController::class, 'index'])
        ->name('discovery.businesses');

Route::get('/events', [EventDiscoveryController::class, 'index'])
        ->name('discovery.events');

Route::get('/b/{business:slug}', [BusinessDiscoveryController::class, 'show'])
        ->name('discovery.business.show');

Route::get('/b/{business:slug}/reservations', [BusinessDiscoveryController::class, 'bookingPage'])
        ->name('discovery.businesses.booking-page');
        // ->middleware('auth');

// Route::get('/b/{business:slug}/make-reservation', [BookingsController::class, 'store'])
//         ->name('users.reservations.store')
//         ->middleware('auth');

Route::get('/events/{event:code}', [EventDiscoveryController::class, 'show'])
        ->name('search.events.show');

Route::post('/events/{event:code}/book', [GuestEventReservationController::class, 'store'])
        ->name('guests.events.reservations.store');

// Route::get('/reservations/{reservation:code}/confirmation', [ReservationController::class, 'confirmation'])
//         ->name('reservations.confirmation');


Route::get('/flutterwave/redirect', [PaymentRedirectController::class, 'flutterwavePaymentRedirect'])
        ->name('flutterwave-payment-redirect');

Route::get('/paystack/redirect', [PaymentRedirectController::class, 'paystackPaymentRedirect'])
        ->name('paystack-payment-redirect');

Route::prefix('/reviews')->middleware(['auth'])->group(function() {
        Route::post('/', [ReviewController::class, 'store'])
                ->name('reviews.store');
                
        Route::delete('/{review}', [ReviewController::class, 'destroy'])
                ->name('reviews.destroy');
});

Route::group(['prefix' => 'users', 'as' => 'users.', 'middleware' => ['auth']], function() {
        
        Route::get('/bookings', [BookingsController::class, 'index'])
                ->name('bookings');

        // 'GET' because we direct her after authentication. url.intended only works with get routes
        Route::get('/bookings/store', [BookingsController::class, 'store'])
                ->name('bookings.store');

        Route::get('/bookings/{booking}/show', [BookingsController::class, 'show'])
                ->name('bookings.show');

        Route::get('/bookings/{reservation:code}/confirmation', [BookingsController::class, 'confirmation'])
                ->name('bookings.confirmation');

        Route::get('/event-tickets', [EventTicketController::class, 'index'])
                ->name('tickets');

        Route::get('/event-tickets/{ticket}', [EventTicketController::class, 'show'])
                ->name('tickets.show');

        Route::post('/event-tickets', [EventTicketController::class, 'post'])
                ->name('tickets.store');

        Route::get('/profile', [ProfileController::class, 'edit'])
                ->name('profile');

        Route::post('/profile/update', [ProfileController::class, 'update'])
                ->name('profile.update');

        Route::delete('/profile-photo', [ProfilePhotoController::class, 'destroy'])
                ->name('profile-photo.destroy');

        Route::delete('/profile', [ProfileController::class, 'destroy'])
                ->name('profile.destroy');

        // Route::post('/profile/update', [Passwordupdacon::class, 'update'])
        //         ->name('users.update');

        // Route::post('/password/update', [EventTicketController::class, 'update'])
        //         ->name('users.update');

});


Route::get('/privacy', [LegalController::class, 'privacy'])
        ->name('privacy');

Route::get('/terms', [LegalController::class, 'terms'])
        ->name('terms');

require __DIR__.'/auth.php';
