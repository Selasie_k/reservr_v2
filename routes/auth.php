<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\VerifyEmailController;
use App\Http\Controllers\Auth\RegisteredUserController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\UserRegistration\NewUserPasswordController;
use App\Http\Controllers\Auth\UserRegistration\VerifiedPhoneController;
use App\Http\Controllers\Auth\UserRegistration\RegistrationSessionController;

Route::get('/register/session', [RegistrationSessionController::class, 'create'])
                ->middleware('guest')
                ->name('registration-session.create');

Route::post('/register/session', [RegistrationSessionController::class, 'store'])
                ->middleware('guest')
                ->name('registration-session.store');

Route::get('/register/verify-phone', [VerifiedPhoneController::class, 'create'])
                ->middleware('guest')
                ->name('registration-session.verify-phone');

Route::post('/register/verify-phone', [VerifiedPhoneController::class, 'store'])
                ->middleware('guest')
                ->name('registration-session.verify-phone.store');

Route::get('/register/verify-phone/resend-code', [VerifiedPhoneController::class, 'resend'])
                ->middleware(['guest', 'throttle:2,1'])
                ->name('registration-session.verify-phone.resend-code');

Route::get('/register/password', [NewUserPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('registration-session.password.create');

Route::post('/register/password', [NewUserPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('registration-session.password.store');


// ****************************************************************
Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware('guest')
                ->name('register');

Route::post('/register', [RegisteredUserController::class, 'store'])
                ->middleware('guest');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
                ->middleware('guest')
                ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
                ->middleware('guest');

Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware('guest')
                ->name('password.request');

Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
                ->middleware('guest')
                ->name('password.email');

Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware('guest')
                ->name('password.reset');

Route::post('/reset-password', [NewPasswordController::class, 'store'])
                ->middleware('guest')
                ->name('password.update');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware('auth')
                ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
                ->middleware(['auth', 'signed', 'throttle:6,1'])
                ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
                ->middleware(['auth', 'throttle:6,1'])
                ->name('verification.send');

Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
                ->middleware('auth')
                ->name('password.confirm');

Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
                ->middleware('auth');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
                ->middleware('auth')
                ->name('logout');
