@component('mail::message')
# Reservation Created

Your reservation has been created|

@component('mail::button', ['url' => route('reservations.confirmation', ['reservation' => $reservation])])
View Appointment
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent