export default {
    methods: {
        extractAddress(e, place_name = null){
            const address_components = e.address_components
            return {
                address: (place_name ? `${place_name}, ` : ``) + e.formatted_address,
                country: this.extractAddressComponent(address_components, 'country'),
                region: this.extractAddressComponent(address_components, 'administrative_area_level_1'),
                city: this.extractAddressComponent(address_components, 'locality'),
                lat: e.geometry.location.lat(),
                lng: e.geometry.location.lng(),
            } 
        },
        extractAddressComponent(components, type){
            let filtered = components.filter(a => a.types.includes(type))
            if(!filtered.length) return null
            return filtered[0].long_name
        },
    }
}