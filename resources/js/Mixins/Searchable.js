export default {
    watch: {
        search: {
            handler(){
                this._search()
            },
            deep: true
        }
    },

    methods: {
        _search: _.debounce(function(){
            this.getData()
        }, 300),
    },
}