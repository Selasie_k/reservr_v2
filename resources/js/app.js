require('./bootstrap');

// require('alpinejs');
import Vue from 'vue'
import VueMeta from 'vue-meta'
import PortalVue from 'portal-vue'
import { App, plugin } from '@inertiajs/inertia-vue'
import { InertiaProgress } from '@inertiajs/progress/src'
import moment from 'moment'
import VCalendar from 'v-calendar';
import * as VueGoogleMaps from 'vue2-google-maps'

require('./vue-tailwind');

Vue.config.productionTip = false
Vue.use(plugin)
Vue.use(PortalVue)
Vue.use(VueMeta)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCRLuAEBTKzhi9JOAiJb1FA95_uUEVdW0k',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
  },
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})

Vue.prototype.$ratingsColors = ['#FCD34D', '#FBBF24','#F59E0B', '#EF4444', '#DC2626']
Vue.prototype.$moment = moment
Vue.prototype.$window = window
Vue.prototype.$document = document
Vue.mixin({ methods: { route: window.route } })

InertiaProgress.init()

const el = document.getElementById('app')

new Vue({
  metaInfo: {
    titleTemplate: (title) => title ? `${title} - RESERVR.CC` : 'RESERVR.CC'
  },
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: name => import(`@/Pages/${name}`).then(module => module.default),
    },
  }),
}).$mount(el)

Vue.prototype.$can = function(abilities){
  var permissions = JSON.parse(el.dataset.page).props.auth.user?.permissions
  if(!permissions){
    return false
  }
  if(typeof abilities == 'string'){
    abilities = [abilities]
  }

  return !!(permissions.filter(i => abilities.includes(i)).length)
}

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
  // componentPrefix: 'vc',  // Use <vc-calendar /> instead of <v-calendar />
});

import Icon from './Shared/Icon'
Vue.component('icon', Icon)

import pagination from './Shared/Pagination'
Vue.component('pagination', pagination)

import FormSection from './Shared/FormSection'
Vue.component('form-section', FormSection)

import SectionLabel from './Shared/SectionLabel'
Vue.component('section-label', SectionLabel)

import Spinner from './Shared/Spinner'
Vue.component('spinner', Spinner)

import Dropdown from './Shared/Dropdown'
Vue.component('dropdown', Dropdown)

Vue.prototype.authUser = function() {
  return JSON.parse(el.dataset.page).props.auth.user || null
}

import datePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
Vue.component('date-time-picker', datePicker);
