<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GuestReservationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function the_guest_business_reservation_page_loads()
    // {
    //     $this->withoutExceptionHandling();

    //     $user = User::factory()->create();

    //     $response = $this->get(route('search.business.show'));

    //     $response->assertStatus(200);
    // }

    /**@test */
    public function a_guest_can_make_a_reservation()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
