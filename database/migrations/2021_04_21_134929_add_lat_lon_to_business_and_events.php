<?php

use App\Traits\DeletesColums;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatLonToBusinessAndEvents extends Migration
{

    use DeletesColums;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropColumnIfExists('businesses', 'position');
        $this->dropColumnIfExists('businesses', 'digital_address');
        $this->dropColumnIfExists('businesses', 'location');
        $this->dropColumnIfExists('businesses', 'city');
        $this->dropColumnIfExists('events', 'position');

        Schema::table('businesses', function (Blueprint $table) {
            $table->string('address');
            $table->string('country');
            $table->string('city');
            $table->text('banner')->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->integer('featured_rating_id')->nullable();
            $table->integer('rating')->default(0);
            $table->integer('reviews_count')->default(0);
            $table->string('website')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->integer('rating')->nullable();
            $table->integer('reviews_count')->nullable();
            $table->string('website')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('current_business_id')->nullable();
            $table->string('profile_photo_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->text('position')->nullable();
            $table->text('digital_address')->nullable();
            $table->text('location')->nullable();
            $table->dropColumn([
                'address', 
                'country', 
                'city', 
                'banner', 
                'lat', 
                'lng', 
                'featured_rating_id', 
                'rating', 
                'reviews_count',
                'website',
                'instagram',
                'facebook',
                'twitter',
            ]);
        });
        
        Schema::table('events', function (Blueprint $table) {
            $table->text('position')->nullable();
            $table->dropColumn(['lat', 'lng', 'rating', 'reviews_count']);
        });
        
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'current_business_id', 
                'profile_photo_path',
                'website',
                'instagram',
                'facebook',
                'twitter',
            ]);
        });
    }
}
