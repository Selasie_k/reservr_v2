<?php

use App\Traits\DeletesColums;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditReservationsTable extends Migration
{
    use DeletesColums;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->dropColumnIfExists('reservations', 'is_client_originated');

        Schema::table('reservations', function (Blueprint $table) {
            $table->enum('initiated_by', ['user', 'business']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropColumn(['initiated_by']);
        });
    }
}
