<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeslotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslots', function (Blueprint $table) {
            $table->id();
            $table->morphs('slotable');
            // $table->string('uid')->unique();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->integer('capacity')->nullable();
            $table->integer('usage')->default(0);
            $table->time('start_time');
            $table->time('end_time');
            // $table->text('note')->nullable();
            // $table->unsignedBigInteger('tenant_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslots');
    }
}
