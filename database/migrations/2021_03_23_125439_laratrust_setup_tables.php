<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LaratrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        // if (DB::getDriverName() !== 'sqlite') {
        //     DB::statement('SET SESSION sql_require_primary_key=0');
        // }
        
        // Clear spatie permissions tables
        Schema::dropIfExists('model_has_permissions');
        Schema::dropIfExists('model_has_roles');
        Schema::dropIfExists('role_has_permissions');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('permissions');


        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for storing teams
        // Schema::create('teams', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('name')->unique();
        //     $table->string('display_name')->nullable();
        //     $table->string('description')->nullable();
        //     $table->timestamps();
        // });

        // Create table for associating roles to users and teams (Many To Many Polymorphic)
        Schema::create('role_user', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('role_id')->constrained('roles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_id')->constrained('teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id');
            $table->string('user_type');

            // $table->foreign('role_id')->references('id')->on('roles')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('team_id')->references('id')->on('teams')
            //     ->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['user_id', 'role_id', 'user_type', 'team_id']);
        });

        // Create table for associating permissions to users (Many To Many Polymorphic)
        Schema::create('permission_user', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('permission_id')->constrained('permissions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_id')->nullable()->constrained('teams')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id');
            $table->string('user_type');

            // $table->foreign('permission_id')->references('id')->on('permissions')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('team_id')->references('id')->on('teams')
            //     ->onUpdate('cascade')->onDelete('cascade');

            $table->unique(['user_id', 'permission_id', 'user_type', 'team_id']);
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('permission_id')->constrained('permissions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('role_id')->constrained('roles')->onUpdate('cascade')->onDelete('cascade');

            // $table->foreign('permission_id')->references('id')->on('permissions')
            //     ->onUpdate('cascade')->onDelete('cascade');
            // $table->foreign('role_id')->references('id')->on('roles')
            //     ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('permission_user');
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('roles');
        // Schema::dropIfExists('teams');
    }
}
