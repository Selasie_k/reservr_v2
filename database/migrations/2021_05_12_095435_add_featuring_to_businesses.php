<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFeaturingToBusinesses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('businesses', function (Blueprint $table) {

            //accepts bookings
            $table->boolean('accepting_bookings')->default(true);

            //admin switch
            $table->boolean('is_active')->default(true);

            //isFeatured
            $table->timestamp('featured_at')->nullable();

            //isClaimed
            $table->timestamp('claimed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function (Blueprint $table) {
            $table->dropColumn(['accepting_bookings', 'is_active', 'featured_at', 'claimed_at']);
        });
    }
}
