<?php

use App\Models\Reservation;
use App\Traits\DeletesColums;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateReservationsTable extends Migration
{
    use DeletesColums;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Reservation::truncate();

        $this->dropColumnIfExists('reservations', 'reservable_type');
        $this->dropColumnIfExists('reservations', 'reservable_id');
        $this->dropColumnIfExists('reservations', 'count');
        $this->dropColumnIfExists('reservations', 'slot_id');
        $this->dropColumnIfExists('reservations', 'must_purchase');

        Schema::table('reservations', function (Blueprint $table) {
            $table->foreignId('business_id')->constrained('businesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropColumn(['business_id']);
        });
    }
}
