<?php

use App\Models\Package;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('price');
            $table->string('bookings');
            $table->timestamps();
        });

        Package::create(['name' => 'Free', 'price' => 0, 'bookings' => 10]);
        Package::create(['name' => 'Basic', 'price' => 4900, 'bookings' => 100]);
        Package::create(['name' => 'Business', 'price' => 9900, 'bookings' => 200]);
        Package::create(['name' => 'Premium', 'price' => 19900, 'bookings' => 500]);
        Package::create(['name' => 'Enterprise', 'price' => 49900, 'bookings' => 1000]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
