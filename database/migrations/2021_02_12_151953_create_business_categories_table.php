<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Category::create(['name' => 'Restaurant & Lounge']);
        Category::create(['name' => 'Medical & Health Facilities']);
        Category::create(['name' => 'Hair & Beauty']);
        Category::create(['name' => 'Financial Institution']);
        Category::create(['name' => 'Events/Event Planner']);
        Category::create(['name' => 'Business']);
        Category::create(['name' => 'Government Institution']);
        Category::create(['name' => 'Media']);
        Category::create(['name' => 'Garage']);
        Category::create(['name' => 'Legal Services']);
        Category::create(['name' => 'Government Institution']);
        Category::create(['name' => 'Education ']);
        Category::create(['name' => 'Religious Institutions']);
        Category::create(['name' => 'Telecommunication']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
