<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->morphs('eventable');
            $table->foreignId('event_id')->nullable();
            $table->string('code')->unique()->index();
            $table->string('name');
            $table->date('date');
            $table->text('description')->nullable();
            $table->string('location')->nullable();
            $table->string('digital_address')->nullable();
            $table->string('position')->nullable();
            $table->boolean('is_published')->default(0);
            $table->string('limit_per_user')->nullable();
            $table->timestamp('start_accepting_at')->nullable();
            $table->timestamp('stop_accepting_at')->nullable();
            $table->boolean('must_confirm')->default(false);
            $table->unsignedBigInteger('tenant_id')->nullable();
            $table->timestamps();
            // $table->boolean('published')->default(false); //hide event from users
            // $table->string('is_recurring')->default('none');
            // $table->datetime('until')->nullable();
            // $table->string('rrule')->nullable();
            // $table->text('slots_template')->nullable();
            // $table->text('note')->nullable();
            // $table->string('cancellation_buffer')->nullable(); 
            // $table->string('note_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
