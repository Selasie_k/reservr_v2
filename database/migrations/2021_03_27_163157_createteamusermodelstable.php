<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Createteamusermodelstable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if (DB::getDriverName() !== 'sqlite') {
        //     DB::statement('SET SESSION sql_require_primary_key=0');
        // }

        Schema::create('business_user', function (Blueprint $table) {
            $table->foreignId('business_id')->constrained('businesses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_id')->constrained('teams')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['business_id', 'user_id']);
        });

        Schema::create('event_user', function (Blueprint $table) {
            $table->foreignId('event_id')->constrained('events')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_id')->constrained('teams')->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['event_id', 'user_id']);
        });

        Schema::table('team_invites', function (Blueprint $table) {
            $table->text('businesses')->nullable();
            $table->text('events')->nullable();
        });

        Schema::table('team_user', function (Blueprint $table) {
            $table->text('businesses')->nullable();
            $table->text('events')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_user');
        Schema::dropIfExists('event_user');

        Schema::table('team_invites', function (Blueprint $table) {
            $table->dropColumn('businesses');
            $table->dropColumn('events');
        });
        
        Schema::table('team_user', function (Blueprint $table) {
            $table->dropColumn('businesses');
            $table->dropColumn('events');
        });
    }
}
