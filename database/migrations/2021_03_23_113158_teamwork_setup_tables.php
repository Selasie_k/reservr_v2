<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeamworkSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if (DB::getDriverName() !== 'sqlite') {
        //     DB::statement('SET SESSION sql_require_primary_key=0');
        // }

        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->integer('owner_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        Schema::create('team_user', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('team_id')->constrained('teams')->onDelete('cascade');
            $table->timestamps();

            $table->primary(['user_id', 'team_id']);
        });

        Schema::create('team_invites', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('team_id')->constrained('teams')->onDelete('cascade');
            $table->enum('type', ['invite', 'request']);
            $table->string('email');
            $table->string('accept_token');
            $table->string('deny_token');
            $table->timestamps();

            // $table->primary(['user_id', 'team_id']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('current_team_id')->unsigned()->nullable();
        });

        Schema::table('businesses', function (Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->integer('team_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('current_team_id');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('team_id');
        });
        Schema::table('businesses', function (Blueprint $table) {
            $table->dropColumn('team_id');
        });

        Schema::table('team_user', function (Blueprint $table) {
            if (DB::getDriverName() !== 'sqlite') {
                $table->dropForeign(\Config::get('teamwork.team_user_table').'_user_id_foreign');
            }
            if (DB::getDriverName() !== 'sqlite') {
                $table->dropForeign(\Config::get('teamwork.team_user_table').'_team_id_foreign');
            }
        });

        Schema::drop('team_user');
        Schema::drop('team_invites');
        Schema::drop('teams');
    }
}
