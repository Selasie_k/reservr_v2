<?php

namespace Database\Factories;

use App\Models\PhoneVerification;
use Illuminate\Database\Eloquent\Factories\Factory;

class PhoneVerificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PhoneVerification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
