<?php

namespace Database\Factories;

use App\Models\BusinessAvailability;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessAvailabilityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BusinessAvailability::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
