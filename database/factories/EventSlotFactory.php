<?php

namespace Database\Factories;

use App\Models\EventSlot;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventSlotFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EventSlot::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
