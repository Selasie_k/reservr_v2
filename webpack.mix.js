const cssImport = require('postcss-import')
const cssNesting = require('postcss-nesting')
const mix = require('laravel-mix')
const path = require('path')
const purgecss = require('@fullhuman/postcss-purgecss')
const tailwindcss = require('tailwindcss')
var S3Plugin = require('webpack-s3-plugin')
// require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .postCss('resources/css/app.css', 'public/css/app.css')
    .options({
        postCss: [
            cssImport(),
            cssNesting(),
            tailwindcss('tailwind.config.js'),
            ...mix.inProduction() ? [
                purgecss({
                    // content: ['./resources/views/**/*.blade.php', './resources/js/**/*.vue'],
                    content: ['./public/js/*.js'],
                    defaultExtractor: content => content.match(/[\w-/:.]+(?<!:)/g) || [],
                    whitelistPatternsChildren: [/nprogress/],
                }),
            ] : [],
        ],
    })
    .webpackConfig({
        output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
        resolve: {
            alias: {
                vue$: 'vue/dist/vue.runtime.esm.js',
                '@': path.resolve('resources/js'),
                '@Mixins': path.resolve('resources/js/Mixins'),
            },
        },
    })
    .version()
    .sourceMaps()

    // if(mix.inProduction()){
    //     mix.webpackConfig({
    //         plugins: [
    //             new S3Plugin({
    //               include: /.*\.(css|js)/,
    //               s3Options: {
    //                 accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    //                 secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    //                 region: process.env.AWS_DEFAULT_REGION,
    //               },
    //               s3UploadOptions: {
    //                 Bucket: process.env.AWS_BUCKET,
    //                 CacheControl: 'public, max-age=31536000'
    //               },
    //               directory: 'public'
    //             })
    //           ]
    //     })
    // }

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//     require('postcss-import'),
//     require('tailwindcss'),
//     require('autoprefixer'),
// ]);
