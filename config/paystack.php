<?php

return [
    'public_key' => env('PAYSTACK_PUB_KEY'),
    'secret_key' => env('PAYSTACK_SEC_KEY'),
];
