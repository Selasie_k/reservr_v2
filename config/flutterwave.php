<?php

return [
    'public_key' => env('FLUTTERWAVE_PUB_KEY'),
    'secret_key' => env('FLUTTERWAVE_SEC_KEY'),
    'encription_key' => env('FLUTTERWAVE_ENC_KEY')
];
