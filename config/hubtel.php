<?php

return [
    'client_id' => env('HUBTEL_CLIENT_ID'),
    'client_secret' => env('HUBTEL_CLIENT_SECRET'),
    'endpoint' => "https://smsc.hubtel.com/v1/messages/send",
];