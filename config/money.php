<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Laravel money
     |--------------------------------------------------------------------------
     */
    'locale' => config('app.locale', 'en_US'),
    'defaultCurrency' => config('app.currency', 'GHS'),
    'currencies' => [
        'iso' => ['GHS'],
        'bitcoin' => ['XBT'],
        'custom' => [
            // 'MY1' => 2,
            // 'MY2' => 3
        ]
    ]
];
