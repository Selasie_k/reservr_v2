const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    // purge: ['./storage/framework/views/*.php', './resources/views/**/*.blade.php'],
    purge: ['./public/js/*.js'],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/line-clamp')],
};
