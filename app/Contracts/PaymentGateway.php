<?php

namespace App\Contracts;

interface PaymentGateway
{
    public static function paymentLink(array $data);
}