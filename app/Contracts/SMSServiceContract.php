<?php

namespace App\Contracts;

interface SMSServiceContract
{
    public function from($from);

    public function to($to);

    public function message($message);

    public function send();
}