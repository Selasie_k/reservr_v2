<?php

namespace App\Notifications;

use App\Models\Reservation;
use Illuminate\Bus\Queueable;
use App\Notifications\Messages\SMSMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramFile;
use NotificationChannels\Telegram\TelegramMessage;

class UsersReservationConfirmationNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $reservation;

    public function __construct(Reservation $reservation)
    {
        $this->reservation = $reservation;
    }

    public function via($notifiable)
    {
        return ['sms', 'mail', TelegramChannel::class];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->success()
                    ->subject("Booking confirmation for {$this->reservation->reservable->name}")
                    ->greeting("Hello {$this->reservation->first_name},")
                    ->line("This is to confirm your reservation with {$this->reservation->reservable->name} for {$this->reservation->date->format('l, F j, Y \a\t h:i A')}.")
                    ->line("Your booking code is #{$this->reservation->code}.")
                    ->line("Please report promply to the venue and submit the booking code above for admitance.")
                    ->line('Thank you for using our application!');
    }

    public function toSMS($notifiable)
    {
        return (new SMSMessage)
                ->line("Hello {$this->reservation->first_name}")
                ->line("Your booking has been made for {$this->reservation->reservable->name}")
                ->space()
                ->line("Booking code: {$this->reservation->code}")
                ->line("Appointment date: {$this->reservation->date->toDayDateTimeString()}")
                ->space()
                ->line("Kindly note that you will be required to submit the booking code above for admitance. Thank you for using Reservr");
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to(config('services.telegram-bot-api.chat-id'))
            ->content("*Reservation Notice*\nBusiness: {$this->reservation->reservable->name}\nReservee: {$this->reservation->name}\nDate: {$this->reservation->date->toDayDateTimeString()}");
    }
}
