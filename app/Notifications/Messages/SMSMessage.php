<?php

namespace App\Notifications\Messages;


class SMSMessage
{
    public $from;

    public $to;

    public $content;

    public function __construct()
    {
        $this->from = 'Reservr';
    }

    public function from($from)
    {
        $this->from = $from;

        return $this;
    }

    public function to($to = null)
    {
        $this->to = $to;

        return $this;
    }

    public function content($content)
    {
        $this->content = $content;

        return $this;
    }

    public function line($line)
    {
        $this->content = $this->content ? "{$this->content}\n$line" : $line;

        return $this;
    }

    public function space()
    {
        $this->content = "$this->content\n";

        return $this;
    }

}