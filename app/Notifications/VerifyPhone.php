<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Notifications\Messages\SMSMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyPhone extends Notification
{
    /**
     * NOTE!! This notification is not queued because the User model has not been saved into the database yet!!
     */
    use Queueable;

    protected $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function via($notifiable)
    {
        return ['sms'];
    }

    public function toSMS($notifiable)
    {
        return (new SMSMessage)
                ->content("Your Reservr verification code is: {$this->code}");
    }
}
