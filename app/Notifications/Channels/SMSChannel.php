<?php
namespace App\Notifications\Channels;

use App\Clients\MnotifySMS;
use App\Contracts\SmsGatewayInterface;
use App\Contracts\SMSServiceContract;
use Illuminate\Notifications\Notification;
use App\Notifications\Messages\MnotifyMessage;
use App\Services\HubtelSMSService;
use App\Services\MNotifyService;
use Illuminate\Support\Facades\Log;

class SMSChannel 
{
    protected $service;

    public function __construct(SMSServiceContract $service)
    {
        $this->service = $service;
    }

    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('SMS', $notification)) {
            return;
        }

        $message = $notification->toSMS($notifiable);

        return $this->service
                ->from($message->from)
                ->to($to)
                ->message($message->content)
                ->send();
    }
}
