<?php

namespace App\Notifications;

use App\Models\TeamInvite;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TeamInviteEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $invite;

    public function __construct(TeamInvite $invite)
    {
        $this->invite = $invite;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $route = $this->invite->user 
            ? route('users.teams', ['invite_token' => $this->invite->accept_token])
            : route('registration-session.create', ['email' => $this->invite->email, 'invite_token' => $this->invite->accept_token]); 

        return (new MailMessage)
                    ->subject("Invitation to Join {$this->invite->team->display_name} on Reservr.cc")
                    ->greeting("Hello,")
                    ->line("{$this->invite->inviter->name} has invited you to collaborate with them on their team. 
                            Use the button below to login or register to accept the invitation.")
                    ->action('Join team', $route)
                    ->line('Welcome aboard,')
                    ->salutation('Reservr.cc');
    }

}
