<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;

class Availability extends Model
{
    use HasFactory;

    protected $casts = [
        'monday' => 'array',
        'tuesday' => 'array',
        'wednesday' => 'array',
        'thursday' => 'array',
        'friday' => 'array',
        'saturday' => 'array',
        'sunday' => 'array',
        'blocked_dates' => 'array',
    ];

    public function getFullSchedule()
    {
        $fullSchedule = collect([]);
        $daysOfWeek = collect(['monday','tuesday','wednesday','thursday','friday','saturday','sunday']);

        $daysOfWeek->each(function($dow) use ($fullSchedule){
            $fullSchedule->push([
                'day' => $dow,
                'time_instances' => $this->getTimeInstances($this->$dow['open'], $this->$dow['close']),
                'blocked_times' => $this->$dow['blocked_times'],
                'is_available' => $this->$dow['is_available'],
                'capacity' => $this->$dow['capacity'],
            ]);
        });

        return $fullSchedule->groupBy('day')->map(fn($item) => $item->first());
    }

    private function getTimeInstances($startTime, $endTime)
    {
        $start = Carbon::parse($startTime);
        $end = Carbon::parse($endTime);

        //if times are from PM -> AM or equal
        if($start->gte($end)){
            $end = $end->addDay();
        }

        $rule = (new Rule)
                ->setStartDate($start)
                ->setUntil($end)
                ->setInterval($this->minute_interval)
                ->setFreq('MINUTELY');

        return (new ArrayTransformer)
                ->transform($rule)->map(function($recurrence){
                    return $recurrence->getStart()->format('H:i');
                })->toArray();
    }

    public function availableDays()
    {
        return collect(['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'])
                ->filter(fn($day) => $this->$day['is_available']);
    }
}
