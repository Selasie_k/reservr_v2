<?php

namespace App\Models;

use App\Http\Controllers\Users\EventTicketController;
use Carbon\Carbon;
use Cknow\Money\Money;
use App\Models\Purchase;
use App\Traits\Reviewable;
use App\Traits\UsedByTeams;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasFactory, UsedByTeams, Reviewable;

    protected $appends = ['path', 'display_price', 'sales', 'available_slots', 'unseen_bookings_count'];
    
    protected $casts = [
        'is_published' => 'boolean',
        'start_accepting_at' => 'datetime',
        'stop_accepting_at' => 'datetime',
        'date' => 'date',
        // 'time' => 'time',
    ];

    protected static function booted()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->code = strtoupper(\Str::random(5));
        });
        static::created(function ($event) {
            $event->tenant->ownedTeam()->attachModel($event);
        });
    }

    public function eventable()
    {
        return $this->morphTo();
    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class);
    }
    
    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'purchasable');
    }

    public function getAmountToPayFor(Reservation $reservation)
    {
        return $this->price * $reservation->count;
    }

    public function tenant()
    {
        return $this->belongsTo(User::class);
        // return $this->team->owner;
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function scopeSearch($query, array $filters)
    {
        $query->when($filters['name'] ?? null, function($query, $name){
            $query->where('name', 'LIKE', "%$name%");
        });
    }

    public function scopePublished($query)
    {
        $query->whereNotNull('published_at');
    }

    public function scopeActive($query)
    {
        $query->whereDate('date', '>', now());
    }

    public function getUnseenBookingsCountAttribute()
    {
        return $this->tickets()->where('seen', 0)->count();
    }

    public function getDateAttribute($date)
    {
        $date = Carbon::parse($date)->format('Y-m-d');
        return Carbon::parse( $date.' '. $this->time)->toDateTimeString();
    }

    public function getSalesAttribute()
    {
        $amount = $this->purchases()->where('status', 'successful')->sum('amount');
        return Money::GHS($amount * 100)->format();;
    }

    public function getAvailableSlotsAttribute()
    {
        return $this->capacity - $this->tickets()->count();
    }

    public function publish()
    {
        return $this->update([
            'published_at' => now()
        ]);
    }

    public function unpublish()
    {
        return $this->update([
            'published_at' => null
        ]);
    }

    public function getPathAttribute()
    {
        return route('search.events.show', ['event' => $this->code]);
    }

    public function setPriceAttribute($value)
    {
        return $this->attributes['price'] = $value * 100;
    }

    public function setPosterAttribute(UploadedFile $file = null)
    {
        if($file){
            Storage::delete(explode('.com/', $this->poster)[1] ?? '');
            $path = $file->storePublicly('events', 's3');
            return $this->attributes['poster'] = Storage::url($path);
        }
    }

    public function getPosterAttribute($value)
    {
        return $value ?? '/no-image.svg';
    }

    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    public function getDisplayPriceAttribute()
    {
        return Money::GHS($this->price * 100)->format();
    }

    public function isExpired()
    {
        return Carbon::parse($this->date)->lt(now());
    }
}
