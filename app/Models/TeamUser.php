<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TeamUser extends Pivot
{

    protected $casts = [
        'events' => 'array',
        'businesses' => 'array'
    ];
    
}
