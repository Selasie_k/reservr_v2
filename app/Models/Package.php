<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;

    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'purchasable');
    }

    public static function successfulPurchases()
    {
        return 0;
        // return $self->purchases()->where('status', 'successful');
    }

    public function getPriceAttribute($value)
    {
        return $value / 100;
    }
}
