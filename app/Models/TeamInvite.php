<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Mpociot\Teamwork\TeamInvite as TeamworkTeamInvite;

class TeamInvite extends TeamworkTeamInvite
{
    use HasFactory, Notifiable;
}
