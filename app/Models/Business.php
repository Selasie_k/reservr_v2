<?php

namespace App\Models;

use App\Models\Purchase;
use App\Traits\Reviewable;
use App\Traits\UsedByTeams;
use App\Traits\HasPhoneNumber;
use App\Traits\HasPhotos;
use Illuminate\Http\UploadedFile;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Propaganistas\LaravelPhone\Casts\E164PhoneNumberCast;

class Business extends Model
{
    use HasFactory, HasPhoneNumber, UsedByTeams, Reviewable, HasPhotos;

    protected $appends = ['path', 'unseen_bookings_count'];

    protected $casts = [
        // 'phone' => E164PhoneNumberCast::class.':GH',
    ];

    protected static function booted(){
        static::created(function($business){
            $business->availability()->create([
                'monday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => true, 'blocked_times' => [], 'capacity' => 10
                ],
                'tuesday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => true, 'blocked_times' => [], 'capacity' => 10
                ],
                'wednesday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => true, 'blocked_times' => [], 'capacity' => 10
                ],
                'thursday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => true, 'blocked_times' => [], 'capacity' => 10
                ],
                'friday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => true, 'blocked_times' => [], 'capacity' => 10
                ],
                'saturday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => false, 'blocked_times' => [], 'capacity' => 10
                ],
                'sunday' => [
                    'open' => '9:00 AM', 'close' => '5:00 PM', 'is_available' => false, 'blocked_times' => [], 'capacity' => 10
                ],
                'blocked_dates' => []
            ]);

            $business->tenant->ownedTeam()->attachModel($business);

            // $business->slug = \Str::slug($business->name);
        });

        static::creating(function($model){
            $model['slug'] = \Str::slug($model->name);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tenant()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function owner()
    {
        return $this->tenant;
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function availability()
    {
        return $this->hasOne(Availability::class);
    }

    public function reservations()
    {
        // return $this->morphMany(Reservation::class, 'reservable');
        return $this->hasMany(Reservation::class);
    }

    public function services()
    {
        return $this->hasMany(BusinessService::class);
    }

    public function payments()
    {
        return $this->morphMany(Purchase::class, 'purchasable');
    }

    public function getAmountToPayFor(Reservation $reservation)
    {
        return $reservation->amount;
    }

    public function getUnseenBookingsCountAttribute()
    {
        return $this->reservations()->where('seen', 0)->count();
    }

    public function getLogoAttribute($logo)
    {
        return $logo ?? '/no-image.svg';
    }

    public function getBannerAttribute($banner)
    {
        return $banner ?? $this->logo;
    }

    public function scopeSearch($query, array $filters)
    {
        $query->when($filters['name'] ?? null, function($query, $name){
            $query->where('name', 'LIKE', "%$name%")->orWhere('category', 'LIKE', "%$name%");
        })->when($filters['categories'] ?? null, function($query, $categories){
            if(count($categories)){
                $query->whereIn('category', $categories);
            }
        })->when($filters['featured_at'] ?? null, function($query, $featured_at){
            $query->whereNotNull('featured_at');
        });
    }

    public static function scopeOnline($query)
    {
        $query->where('is_online', 1);
    }

    public static function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function getPathAttribute()
    {
        return route('discovery.business.show', ['business' => $this->slug]);
    }

    public function hasInsufficientBalance()
    {
        return $this->user->balance < 1;
    }

    public function goOnline()
    {
        return $this->update(['is_online' => true]);
    }

    public function goOffline()
    {
        return $this->update(['is_online' => false]);
    }

    public function purge()
    {
        //delete pictures
        //delete services
        $this->services()->delete();

        //delete reviews
        $this->reviews()->delete();

        //reset business id on user record

        //delete schedule
        $this->availability()->delete();
        
        $this->delete();

        //Fire Event

        return 1;
    }
}
