<?php

namespace App\Models;

use App\Contracts\PaymentGateway;
use App\Services\Flutterwave;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Purchase extends Model
{
    use HasFactory;

    public function tenant()
    {
        return $this->belongsTo(User::class);
    }

    public function purchasable()
    {
        return $this->morphTo();
    }

    public function purchaser()
    {
        return $this->morphTo();
    }

    public function getPaymentLink()
    {
        $paymentProvider = app()->make(PaymentGateway::class);

        return $paymentProvider::paymentLink([
            'ref' => $this->ref,
            'amount' => $this->amount, // in cedis
            'name' => $this->purchaser->name,
            'email' => $this->purchaser->email,
            'phone' => $this->purchaser->phone,
            'description' => $this->description,
        ]);
    }

    public function isSuccessful()
    {
        return $this->status == 'successful';
    }
}
