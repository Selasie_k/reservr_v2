<?php

namespace App\Models;

use Laratrust\Models\LaratrustTeam;
use Mpociot\Teamwork\TeamworkTeam;

class Team extends TeamworkTeam
{
    public $guarded = [];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

    public function attachModel($model)
    {
        return $model->update(['team_id' => $this->id]);
    }
}
