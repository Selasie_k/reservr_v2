<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Event;
use App\Models\Business;
use App\Traits\HasPhoneNumber;
use App\Events\ReservationCreated;
use App\Events\ReservationCancelled;
use App\Events\ReservationConfirmed;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reservation extends Model
{
    use HasFactory, HasPhoneNumber, Notifiable;

    // protected $with = ['slot'];

    protected $appends = ['status', 'name', 'date'];

    protected $casts = [
        'appointment_date' => 'date',
        'services' => 'array',
    ];

    protected static function booted()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->code = strtoupper(\Str::random(3)) .''. rand(1000, 9999);
        });
    }
    
    // public function routeNotificationForMNotify($notifiable)
    // {
    //     return $this->phone;
    // }
    
    public function routeNotificationForSMS($notifiable)
    {
        return $this->phone;
    }

    public function tenant()
    {
        return $this->belongsTo(User::class);
    }

    // public function reservable()
    // {
    //     return $this->morphTo();
    // }

    public function business()
    {
        return $this->belongsTo(Business::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->morphOne(Purchase::class, 'purchaser');
    }

    public function getDateAttribute()
    {
        $date = $this->appointment_date->format('Y-m-d');
        $time =  $this->appointment_time;
        return Carbon::parse("$date $time");
    }

    public function getNameAttribute()
    {
        return "$this->first_name $this->last_name";
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['q'] ?? null, function ($query, $q) {
            $query->where('last_name', 'LIKE', "%$q%")
                ->orWhere('code', 'LIKE', "%$q%");
        })->when($filters['date'] ?? null, function ($query, $date) {
            return ! isset($date[1])
            ? $query->whereDate('appointment_date', '=', $date[0])
            : $query->whereDate('appointment_date', '>=', $date[0])->whereDate('appointment_date', '<=', $date[1]);
        })->when($filters['sortBy'] ?? null, function ($query, $sortBy) {
            $query->orderBy($sortBy, 'desc');
        });
    }

    public function getStatusAttribute()
    {
        if($this->declined_at){
            return 'declined';
        }

        if($this->cancelled_at){
            return 'cancelled';
        }

        if(!$this->confirmed_at){
            return 'pending';
        }

        return 'confirmed';
    }

    public function markAsSeen()
    {
        return $this->update(['seen' => true]);
    }

    public function checkIn()
    {
        return $this->update(['checked_in_at' => now()]);
    }

    public function confirm()
    {
        $this->update([ 'confirmed_at' => now() ]);

        event(new ReservationConfirmed($this));

        return [
            'status' => 'success',
            'message' => 'Reservation confirmed successfully!'
        ];
    }

    public function cancel()
    {
        $this->update([ 'cancelled_at' => now() ]);

        //FIRE notification
        event(new ReservationCancelled($this));

        return [
            'status' => 'success',
            'message' => 'Reservation cancelled successfully!'
        ];
    }

    public function decline()
    {
        $this->update([ 'declined_at' => now() ]);

        //FIRE notification

        return [
            'status' => 'success',
            'message' => 'Reservation declined successfully!'
        ];
    }

    public function amountToPay()
    {
        // return $this->reservable->getAmountToPayFor($this);
        return $this->business->getAmountToPayFor($this);
    }

    // public function getPaymentLinkFor($reservable_type, $reservable_id)
    public function getPaymentLink()
    {
        return $this->payment()->create([
            'purchasable_type' => 'App\Models\Business', // [Event, Business]
            'purchasable_id' => $this->business->id,
            'tenant_id' => $this->tenant->id,
            'description' => "Payment for booking to {$this->business->name} on reservr.cc",
            'amount' => $this->amountToPay(),
            'ref' => time() .'.'. \Str::random(15),
            'status' => 'pending'
        ])->getPaymentLink();
    }

    public function handlePaymentRedirect(Purchase $purchase)
    {
        if($purchase->isSuccessful()){

            $purchase->tenant->debit();

            event(new ReservationCreated($this));

            return redirect()->route('users.bookings.show', ['booking' => $this]);
        }

        // $routes = [
        //     Event::class => fn() => route('search.events.show', ['event' => $purchase->purchasable]), 
        //     Business::class => fn() => route('search.businesses.show', ['business' => $purchase->purchasable])
        // ];

        // $path =  $routes[$purchase->purchasable_type]();

        $this->delete();
        //TODO: Delete purchase record too?

        return redirect()->route('discovery.businesses.booking-page', ['business' => $purchase->purchasable])->with('error', 'Payment attempt failed. Please try again');
    }
}
