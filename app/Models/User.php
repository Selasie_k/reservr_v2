<?php

namespace App\Models;

use App\Models\Purchase;
use App\Traits\AttachesToTeamModels;
use App\Traits\HasBusinesses;
use App\Traits\HasPhoneNumber;
use App\Traits\HasProfilePhoto;
use App\Traits\HasPurchases;
use App\Traits\HasTeams;
use App\Traits\MustVerifyPhone;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Propaganistas\LaravelPhone\PhoneNumber;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Propaganistas\LaravelPhone\Casts\E164PhoneNumberCast;

class User extends Authenticatable implements MustVerifyEmail
{
    use 
        HasTeams,
        HasFactory, 
        Notifiable, 
        SoftDeletes,
        HasPurchases,
        HasBusinesses,
        HasPhoneNumber, 
        MustVerifyPhone, 
        HasProfilePhoto, 
        LaratrustUserTrait, 
        AttachesToTeamModels;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = ['name', 'profile_photo_url'];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
        // 'phone' => E164PhoneNumberCast::class.':GH',
    ];

    public function routeNotificationForSMS($notifiable)
    {
        return $this->phone;
    }

    public function setPasswordAttribute($value)
    {
        if( !(strlen($value) == 60 && preg_match('/^\$2y\$/', $value ))){ //Password is not already hashed. Edge case for password resets
            $value = bcrypt($value);
        }
        return $this->attributes['password'] = $value;
    }

    public function getAllPermissions()
    {
        return $this
                ->roles->load('permissions')
                ->map(fn($role) => $role->permissions)
                ->flatten()->pluck('name')->unique();
    }

    public function getNameAttribute()
    {
        return "$this->first_name $this->last_name";
    }

    public function handlePaymentRedirect(Purchase $purchase)
    {
        if($purchase->isSuccessful()){
            $this->credit($purchase->purchasable->bookings);
            return redirect()->route('partners.purchases')->with('success', 'Payment successful. Account credited');
        }
        return redirect()->route('partners.purchases')->with('error', 'Payment attempt failed. Please try again');
    }

    public function isAdmin()
    {
        return $this->type == 'root';
    }

    public function scopeSearch($query, array $filters)
    {
        $query->when($filters['name'] ?? null, function($query, $name){
            $query->where('first_name', 'LIKE', "%$name%")
                ->orWhere('last_name', 'LIKE', "%$name%");
        });
    }

    public function bookings()
    {
        return $this->hasMany(Reservation::class);
    }

    public function tickets()
    {
        return $this->hasMany(Reservation::class)->where('reservable_type', 'App\Models\Event');
    }

    public function isPartner()
    {
        return $this->businesses()->count() > 0;
    }

    public function currentBusiness()
    {
        return $this->current_business_id 
                ? $this->businesses()->find($this->current_business_id) 
                : $this->businesses()->first();
    }
}
