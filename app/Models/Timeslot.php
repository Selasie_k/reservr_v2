<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    use HasFactory;
    
    protected static function booted()
    {
        parent::boot();
        // static::creating(function ($model) {
        //     $model->uid = \Str::random(15);
        // });
    }

    public function slotable()
    {
        return $this->morphTo();
    }
}
