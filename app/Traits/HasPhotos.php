<?php

namespace App\Traits;

use App\Models\Photo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait HasPhotos {

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }

    public function uploadPhoto(UploadedFile $photo, $isLogo)
    {
        $path = $photo->storePublicly($isLogo ? 'business-logos' : 'business-photos', ['disk' => $this->photoDisk()]);
        
        $url = Storage::disk($this->photoDisk())->url($path);

        if($isLogo){
            Storage::delete(explode('.com/', $this->logo)[1] ?? '');
            $this->update(['logo' => $url]);
            return;
        }

        $this->photos()->create(['path' => $path, 'url' => $url]);
    }

    protected function photoDisk()
    {
        return config('filesystems.default', 'public');
    }
}