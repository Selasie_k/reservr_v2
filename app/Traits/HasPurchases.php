<?php

namespace App\Traits;

use App\Models\Package;
use App\Models\Purchase;

trait HasPurchases {
    public function purchases()
    {
        return $this->morphMany(Purchase::class, 'purchaser');
    }

    public function transactions()
    {
        return $this->hasMany(Purchase::class, 'tenant_id');
    }

    public function purchase(Package $package)
    {
        return $this->purchases()->create([
            'purchasable_type' => Package::class,
            'purchasable_id' => $package->id,
            'tenant_id' => $this->id,
            'description' => "Payment for $package->bookings bookings on reservr.cc",
            'amount' => $package->price,
            'ref' => time() .'.'. \Str::random(15),
            'status' => 'pending'
        ])->getPaymentLink();
    }
}