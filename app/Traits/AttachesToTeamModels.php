<?php

namespace App\Traits;

use App\Models\Team;
use App\Models\Event;
use App\Models\Business;
use App\Models\TeamUser;
use Illuminate\Support\Facades\DB;

trait AttachesToTeamModels
{
    public function teamEvents($team = null)
    {
        $query = $this->belongsToMany(Event::class, 'event_user');
        if($team){
            return $query->wherePivot('team_id', $team->id);
        }
        return $query->wherePivot('team_id', $this->currentTeam->id);
    }

    public function teamBusinesses($team = null)
    {
        $query = $this->belongsToMany(Business::class, 'business_user');
        if($team){
            return $query->wherePivot('team_id', $team->id);
        }
        return $query->wherePivot('team_id', $this->currentTeam->id);
    }

    public function associateTeamEventsWithUser($events, $team)
    {
        $events = is_array($events) ? $events : json_decode($events);

        //detach from all existing attachments to this team
        // $this->teamEvents($team)->detach(
        //     $this->teamEvents($team)->get(['id','code'])->keyBy('id')->keys()->toArray()
        // );
        $this->teamEvents($team)->detach();

        //reatach to this new ones
        $this->teamEvents()->syncWithoutDetaching(
            $this->appendTeamIdToPivotTable($events, $team->id) 
        );

        //update team_user pivot table
        $this->teams()->first()->pivot->update(['events' => $events ]);
    }

    public function associateTeamBusinessesWithUser($businesses, $team)
    {
        $businesses = is_array($businesses) ? $businesses : json_decode($businesses);

        // detach from all existing attachments to this team
        // $this->teamBusinesses()->detach(
        //     $this->teamBusinesses($team)->get(['id','slug'])->keyBy('id')->keys()->toArray()
        // );
        $this->teamBusinesses($team)->detach();

        $this->teamBusinesses()->syncWithoutDetaching( 
            $this->appendTeamIdToPivotTable($businesses, $team->id)
        );

        //update team_user pivot table
        $this->teams()->where('team_id', $team->id)->first()->pivot->update(['businesses' => $businesses ]);
    }

    private function appendTeamIdToPivotTable(array $modelIDs, int $team_id)
    {
        //convert [1,...] to [1 => ['team_id' => 1],...]
        return collect($modelIDs)->map(function($item) use ($team_id) {
            return ['id' => $item, 'value' => ['team_id' => $team_id]];
        })
        ->keyBy('id')->map(fn($item) => $item['value']);
    }
}
