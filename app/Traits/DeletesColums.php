<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


trait DeletesColums {
    private function dropColumnIfExists($myTable, $column)
    {
        if (Schema::hasColumn($myTable, $column)) 
        {
            Schema::table($myTable, function (Blueprint $table) use ($column)
            {
                $table->dropColumn($column);
            });
        }
    }
}