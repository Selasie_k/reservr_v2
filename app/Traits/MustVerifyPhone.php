<?php

namespace App\Traits;

use App\Models\PhoneVerification;
use App\Notifications\VerifyPhone;

trait MustVerifyPhone
{

    public function hasVerifiedPhone()
    {
        return ! is_null($this->phone_verified_at);
    }

    public function markPhoneAsVerified()
    {
        if(!$this->exists){
            return $this->phone_verified_at = now();
        }

        return $this->forceFill([
            'phone_verified_at' => now(),
        ])->save();
    }

    public function sendPhoneVerificationNotification()
    {
        $this->notify(new VerifyPhone( $this->createVerificationCode() ));
    }

    public function getPhoneForVerification()
    {
        return $this->phone;
    }

    public function hasPendingVerificationCode()
    {
        return !! PhoneVerification::where('phone', $this->getPhoneForVerification())->first();
    }

    private function createVerificationCode()
    {
        return PhoneVerification::create([
                                'phone' => $this->getPhoneForVerification(),
                                'code' => rand(111111, 999999),
                            ])->code;
    }
}
