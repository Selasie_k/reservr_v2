<?php

namespace App\Traits;

use Propaganistas\LaravelPhone\PhoneNumber;

trait HasPhoneNumber
{
    public function getPhoneAttribute($value)
    {
        return (string) PhoneNumber::make($value, 'GH'); 
    }
}
