<?php

namespace App\Traits;

use App\Models\Event;
use App\Models\Business;
use App\Models\Reservation;

trait HasBusinesses {
    
    public function credit($amount = 1)
    {
        return $this->increment('balance', $amount);
    }

    public function debit($amount = 1)
    {
        return $this->decrement('balance', $amount);
    }

    public function hasSufficientBalance()
    {
        return $this->balance > 0;
    }

    public function getTotalBalanceAttribute()
    {
        return $this->balance + $this->bonus;
    }

    public function isClient()
    {
        return $this->hasRole('business owner');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'tenant_id');
    }

    public function businesses()
    {
        return $this->hasMany(Business::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'tenant_id');
    }
    
}