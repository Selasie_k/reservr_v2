<?php

namespace App\Listeners;

use App\Notifications\TeamInviteEmailNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mpociot\Teamwork\Events\UserInvitedToTeam;

class SendInviteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserInvitedToTeam  $event
     * @return void
     */
    public function handle(UserInvitedToTeam $event)
    {
        $invite = $event->getInvite();
        $invite->notify(new TeamInviteEmailNotification($invite));
    }
}
