<?php

namespace App\Listeners;

use App\Events\ReservationCancelled;
use App\Notifications\ReservationCancelledNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReservationCancelledNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReservationCancelled  $event
     * @return void
     */
    public function handle(ReservationCancelled $event)
    {
        return $event->reservation->notify(new ReservationCancelledNotification($event->reservation));
    }
}
