<?php

namespace App\Listeners;

use App\Events\ReservationCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\BusinessReservationNotification;
use App\Notifications\UsersReservationConfirmationNotification;

class SendNewReservationNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReservationCreated  $event
     * @return void
     */
    public function handle(ReservationCreated $event)
    {
        $event->reservation->notify(new UsersReservationConfirmationNotification($event->reservation));
        $event->reservation->tenant->notify(new BusinessReservationNotification($event->reservation));
    }
}
