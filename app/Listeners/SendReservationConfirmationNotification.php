<?php

namespace App\Listeners;

use App\Events\ReservationConfirmed;
use App\Notifications\ReservationConfirmationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReservationConfirmationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReservationConfirmed  $event
     * @return void
     */
    public function handle(ReservationConfirmed $event)
    {
        $event->reservation->notify(new ReservationConfirmationNotification($event->reservation));
    }
}
