<?php

namespace App\Console\Commands;

use App\Models\Business;
use Illuminate\Console\Command;

class UpdateBusinessAvailabilities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'r:update-schedules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dows = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        Business::all()->each(function($business) use ($dows){
            collect($dows)->each(function($dow) use ($business) {
                $day = $business->availability->$dow;
                $day['capacity'] = 10;
                $business->availability->$dow = $day;
                $business->availability->save();
                // dd($business->availability);
            });
        });

        return 0;
    }
}
