<?php

namespace App\Console\Commands;

use App\Models\Team;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SeedTeamsAndRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'r:seed-teams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed Roles, Permissions and Teams';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Team::all()->each->delete();

        User::all()->each(function($user){
            Auth::login($user);
            
            $team = Team::create([
                'owner_id' => $user->getKey(),
                'name' => \Str::slug($user->name).'-'.time(),
                'display_name' => "{$user->name}'s Team"
            ]);

            $user->attachTeam($team);

            $user->switchTeam($team);

            $user->businesses()->each(function($business) use ($team){
                $team->attachModel($business);
            });

            $user->events()->each(function($event) use ($team){
                $team->attachModel($event);
            });            
        });

        return 0;
    }
}
