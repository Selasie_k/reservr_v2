<?php

namespace App\Console\Commands;

use App\Models\Business;
use App\Models\Event;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class TranferImagesToS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reservr:transfer-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move all locally stored images to the s3 bucket for reservr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        Business::all()->each(function($business){
            $this->info('uploading logo for ' . $business->name);

            $file = explode('storage/', $business->logo)[1];

            if ($this->fileExistsInLocalStorage($file)) {
                if ($this->uploadFileToS3Bucket($file)) {
                    $business->logo = Storage::url($file);
                    $business->save();

                    Storage::disk('public')->delete($file);

                    $this->info('done!');
                    $this->newLine();
                }
            } else {
                $this->info('image already uploaded');
            }
        });

        $this->info('Done for businesses. Uploading Event posters');
        
        Event::all()->each(function($event){
            $this->info('uploading poster for ' . $event->name);
            $file = explode('storage/', $event->poster)[1];

            if ($this->fileExistsInLocalStorage($file)) {
                if ($this->uploadFileToS3Bucket($file)) {
                    $event->poster = Storage::url($file);
                    $event->save();

                    Storage::disk('public')->delete($file);

                    $this->info('done!');
                    $this->newLine();
                }
            } else {
                $this->info('image already uploaded');
            }
        });

        $this->info('All uploads completed!');

        return 0;
    }

    private function fileExistsInLocalStorage($file)
    {
        return Storage::disk('public')->exists($file);
    }

    private function uploadFileToS3Bucket($file)
    {
        return Storage::put($file, Storage::disk('public')->get($file), 'public');
    }
}
