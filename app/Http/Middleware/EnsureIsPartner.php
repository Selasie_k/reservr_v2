<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureIsPartner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!Auth::check()){
            return redirect()->route('partners.login');
        }

        if(!$request->user()->isPartner()){
            // redirect to partner signup page
            return redirect()->route('partners.onboarding.create');
            abort(403, 'Sorry, Youre not a partner at reservr.cc. Please create a partner account');
        }

        return $next($request);
    }
}
