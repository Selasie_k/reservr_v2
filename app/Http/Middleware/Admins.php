<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admins
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->user()->isAdmin()){
            abort(403, "You're not authorized to view this page");
        }
        return $next($request);
    }
}
