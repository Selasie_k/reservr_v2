<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MustVerifyPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->user()->phone_verified_at){
            return redirect()->route('verify-phone-number');
        }

        return $next($request);
    }
}
