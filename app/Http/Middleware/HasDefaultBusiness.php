<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class HasDefaultBusiness
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->user()->current_business_id){
            return redirect()->route('partners.auth.business-option');
        }
        return $next($request);
    }
}
