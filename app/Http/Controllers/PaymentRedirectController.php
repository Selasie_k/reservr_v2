<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Package;
use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Services\Flutterwave;

class PaymentRedirectController extends Controller
{
    public function flutterwavePaymentRedirect(Request $request)
    {
        $purchase = Purchase::whereRef($request->tx_ref)->first() ?? abort(404, 'Purchase not found!');

        $purchase->update([
            'transaction_id' => $request->transaction_id == 'null' ? null : $request->transaction_id,
            'status' => $request->status,
            'provider' => 'flutterwave',
        ]);

        return $purchase->purchaser->handlePaymentRedirect($purchase);
    }

    public function paystackPaymentRedirect(Request $request)
    {
        $purchase = Purchase::whereRef($request->trxref)->first() ?? abort(404, 'Purchase not found!');

        $purchase->update([
            'transaction_id' => $request->trxref,
            'status' => 'successful',
            'provider' => 'paystack',
        ]);

        return $purchase->purchaser->handlePaymentRedirect($purchase);
    }
}
