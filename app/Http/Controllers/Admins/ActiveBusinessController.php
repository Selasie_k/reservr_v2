<?php

namespace App\Http\Controllers\Admins;

use App\Models\Business;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActiveBusinessController extends Controller
{
    public function toggle(Business $business)
    {
        if($business->is_active){
            $business->update(['is_active' => false]);
            return redirect()->back()->with('success', 'This business has been suspended');
        }

        $business->update(['is_active' => true]);
        return redirect()->back()->with('success', 'This business has been activated');
    }
}
