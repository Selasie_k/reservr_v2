<?php

namespace App\Http\Controllers\Admins;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Business;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class BusinessController extends Controller
{
    public function index()
    {
        return Inertia::render('Admins/Businesses/Index', [
            'businesses' => Business::latest()->search(request()->all())->with('tenant:id,first_name,last_name,email')->paginate(10),
            'categories' => Category::all()->pluck('name'),
        ]);
    }

    public function create()
    {
        return $this->edit();
    }

    public function edit(Business $business = null)
    {
        return Inertia::render('Admins/Businesses/Form', [
            'business' => $business ?$business->load('tenant:id,first_name,last_name') : (object) [],
            'categories' => Category::all()->pluck('name'),
        ]);
    }

    public function store(Request $request)
    {
        $business = $request->validate([
            'name' => 'required|unique:businesses',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'description' => 'required|max:1000',
            'category' => 'required',

            'address' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'lat' => 'required|max:255',
            'lng' => 'required|max:255',

            'website' => 'nullable|max:255',
            'facebook' => 'nullable|max:255',
            'instagram' => 'nullable|max:255',
            'twitter' => 'nullable|max:255',
        ]);

        $rand = Str::random(5);

        $user = User::create([
            'first_name' => 'annon-user',
            'last_name' => '_'.$rand,
            'email' => 'annon-'. $rand . '@reservr.cc',
            'phone' => $business['phone'],
            'email_verified_at' => now(),
            'phone_verified_at' => now(),
            'password' => Hash::make('user@reservr')
        ]);

        $business = $user->businesses()->create($business);
        $user->update(['current_business_id' => $business->id]);

        return redirect()
                ->route('admins.businesses.edit', ['business' => $business])
                ->with('success', 'Business created!');
    }

    public function update(Request $request, Business $business)
    {
        $business->update($request->validate([
            'name' => ['required', Rule::unique('businesses')->ignore($business->id)],
            'email' => 'required',
            'phone' => 'required',
            'description' => 'required|max:1000',
            'category' => 'required',

            'address' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'lat' => 'required|max:255',
            'lng' => 'required|max:255',

            'website' => 'nullable|max:255',
            'facebook' => 'nullable|max:255',
            'instagram' => 'nullable|max:255',
            'twitter' => 'nullable|max:255',
        ]));

        return redirect()
                ->back()
                ->with('success', 'Business record updated!');
    }

    public function feature(Business $business)
    {
        if($business->featured_at){
            $business->update(['featured_at' => null]);
            return redirect()->back()->with('success', 'This business has been removed from the featured list');
        }

        $business->update(['featured_at' => now()]);
        
        return redirect()->back()->with('success', 'This business has been added to the featured list');
    }

    public function destroy(Business $business)
    {
        $business->purge();

        return redirect()->route('admins.businesses')->with('success', 'Business record deleted');
    }
}
