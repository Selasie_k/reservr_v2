<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Admins/Users/Index', [
            'users' => User::latest()->search(request()->all())->paginate(20)
        ]);
    }

    public function show(User $user)
    {
        return Inertia::render('Admins/Users/Show', [
            'user' => $user->load('businesses','events', 'purchases'),
            'packages' => Package::all()
        ]);
    }
}
