<?php

namespace App\Http\Controllers\Admins;

use App\Models\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class SubCategoryController extends Controller
{

    public function store(Category $cat, Request $request)
    {
        $cat->subCategories()->create($request->validate([
            'name' => 'required|max:255'
        ]));

        return redirect()->back()->with('success', 'subcategory added!');
    }

    public function update(Request $request, SubCategory $subCategory)
    {
        $subCategory->update($request->validate([
            'name' => 'required|max:50'
        ]));

        return redirect()->back()->with('success', 'Category updated');
    }

    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();

        return redirect()->back()->with('success', 'subcategory deleted!');
    }
}
