<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index()
    {
        return Inertia::render('Admins/Categories/Index', [
            'categories' => Category::withCount('subCategories')->get()
        ]);
    }

    public function store(Request $request)
    {
        Category::create($request->validate([
            'name' => 'required|max:50'
        ]));

        return redirect()->back()->with('success', 'Category created');
    }

    public function show(Category $category)
    {
        return Inertia::render('Admins/Categories/Show', [
            'category' => $category->load('subCategories'),
        ]);
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->validate([
            'name' => 'required|max:50'
        ]));

        return redirect()->back()->with('success', 'Category updated');
    }

    public function destroy(Category $category)
    {
        $category->subCategories()->delete();

        $category->delete();

        return redirect()->back()->with('success', 'Category deleted!');
    }
}
