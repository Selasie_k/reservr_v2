<?php

namespace App\Http\Controllers\Admins;

use App\Models\User;
use Inertia\Inertia;
use App\Models\Event;
use App\Models\Business;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Purchase;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Admins/Dashboard/Index', [
            '_stats' => [
                'users' => User::count(),
                'businesses' => Business::count(),
                'events' => Event::count(),
                'reservations' => Reservation::count()
            ],
            'grand_total' => Purchase::where('purchasable_type', Package::class)->sum('amount'),
            // '_packages' => [
            //     [ 'name' => 'free', 'total' => $this->getTotalPurchases('free'), ],
            //     [ 'name' => 'basic', 'total' => $this->getTotalPurchases('basic'), ],
            //     [ 'name' => 'business', 'total' => $this->getTotalPurchases('business'), ],
            // ]
        ]);
    }

    private function getTotalPurchases($package){
        return Package::where('name', 'free')->first()->successfulPurchases()->sum('amount');
    }
}
