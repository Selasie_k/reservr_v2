<?php

namespace App\Http\Controllers\Admins;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserImpersonationController extends Controller
{
    public function create(Request $request, User $user)
    {
        Session::put('impersonating', $user);
        Session::put('return_url', url()->previous());
        Session::put('admin', $request->user());
        Auth::login($user);

        return redirect()->route('partners.dashboard');
    }

    public function destroy()
    {
        if($admin = Session::get('admin')){
            Session::forget('impersonating');
            Auth::login($admin);
            return redirect(Session::pull('return_url'));
        }

        return redirect()->back()->with('error', 'An error occured');
    }
}
