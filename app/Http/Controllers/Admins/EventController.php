<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EventController extends Controller
{
    public function index()
    {
        return Inertia::render('Admins/Events/Index', [
            'events' => Event::latest()->search(request()->all())->paginate(10),
        ]);
    }

    public function show(Event $event)
    {
        return Inertia::render('Admins/Events/Show', [
            'event' => $event->load('tenant:id,first_name,last_name')
        ]);
    }
}
