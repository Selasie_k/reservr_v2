<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Events\ReservationCreated;
use App\Models\Event;

class GuestEventReservationController extends Controller
{
    public function store(Request $request, Event $event)
    {
        // $reservable = $request->reservable_type::find($request->reservable_id);

        if(!$event->tenant->hasSufficientBalance()){
            return redirect()->back()->with('error', 'Balance too low to fulfill this reservation');
        }

        $reservation = $event->reservations()->create($request->validate([
            // 'slot_id' => 'nullable',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|phone:GH',
            'email' => 'required|email',
            'count' => 'required|numeric|min:1',
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'is_client_originated'=> 'boolean',
        ]));

        $reservation->update([
            'tenant_id' => $event->tenant->id,
        ]);

        // if($reservation->is_client_originated){
        //     $reservation->tenant->debit();
        //     event(new ReservationCreated($reservation));

        //     return redirect()->back()->with('success', 'Reservation successfully created!');
        // }

        if($reservation->amountToPay() > 0){
            $paymentLink = $reservation->getPaymentLinkFor(
                    $reservation->reservable_type, // Event, Business
                    $reservation->reservable_id
                );

            return Inertia::location($paymentLink);
        }

        $reservation->tenant->debit();
        event(new ReservationCreated($reservation));

        return redirect()->route('reservations.confirmation', [
            'reservation' => $reservation
        ]);
    }
}
