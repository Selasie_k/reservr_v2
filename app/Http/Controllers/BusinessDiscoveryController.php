<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BusinessDiscoveryController extends Controller
{
    public function index(Request $request)
    {
        $results = Business::allTeams()
                ->online()
                ->active()
                ->search($request->only('name', 'categories'))
                ->orderBy('featured_at', 'desc')
                ->paginate(10);

        return Inertia::render('Front/BusinessDiscovery/Index', [
            'businesses' => tap($results, function($paginatedInstance){
                return $paginatedInstance->getCollection()->transform(function($business, $key){
                        return $this->transformBusiness($business);
                    });
                }), 
            'categories' => Category::all()->pluck('name'),
            'filters' => $request->all()
        ]);
    }

    public function show(Business $business)
    {
        return Inertia::render('Front/BusinessDiscovery/Show', [
            'business' => [
                'id' => $business->id,
                'name' => $business->name,
                'slug' => $business->slug,
                'email' => $business->email,
                'phone' => $business->phone,
                'logo' => $business->logo,
                'banner' => $business->banner,
                'lat' => $business->lat,
                'lng' => $business->lng,
                'category' => $business->category,
                'address' => $business->address,
                'rating' => $business->rating,
                'description' => $business->description,
                'accepting_bookings' => $business->accepting_bookings,
                'photos' => $business->photos()->get('url')->pluck('url'),
                'reviews' => $business->reviews()->with('user:id,first_name,last_name')->get(['id', 'user_id', 'rating', 'review', 'created_at']),
                'website' => $business->website,
                'instagram' => $business->instagram,
                'facebook' => $business->facebook,
                'twitter' => $business->twitter,
            ],
            'schedule' => $business->availability->getFullSchedule(),
            'services' => $business->services,
            'suggestedBusinesses' => Business::allTeams()
                                    ->online()
                                    ->active()
                                    ->where('category', $business->category)
                                    ->where('id', '!=', $business->id)
                                    ->take(5)
                                    ->get()
                                    ->transform(fn($business) => $this->transformBusiness($business))
        ]);
    }

    public function bookingPage(Business $business)
    {
        if(!$business->accepting_bookings){
            return redirect($business->path)->with('error', 'This business is not accepting reservations');
        }

        return Inertia::render('Front/BusinessDiscovery/BookingPage', [
            'business' => [
                'id' => $business->id,
                'name' => $business->name,
                'slug' => $business->slug,
                'logo' => $business->logo,
                'banner' => $business->banner,
                'availability' => $business->availability,
                'tenant' => $business->tenant,
            ],
            'schedule' => $business->availability->getFullSchedule(),
            'services' => $business->services,
        ]);
    }

    private function transformBusiness($business)
    {
        return [
            'name' => $business->name,
            'category' => $business->category,
            'description' => $business->description,
            'path' => $business->path,
            'banner' => $business->banner,
            'address' => $business->address,
            'rating' => $business->rating,
            'featured_at' => $business->featured_at,
            'reviews_count' => $business->reviews_count,
            'tenant_balance' => $business->tenant->balance,
        ];
    }
}
