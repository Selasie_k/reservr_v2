<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{

    public function index()
    {
        return Inertia::render('Partners/Photos/Index', [
            'photos' => auth()->user()->currentBusiness()->photos,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'photo' => ['required', 'file', 'mimes:jpg,jpeg,png', 'max:1024'],
        ]);

        $request->user()->currentBusiness()->uploadPhoto($request->photo, $request->isLogo);

        return redirect()->back()->with('success', 'Photo uploaded');
        // dd($request->all());
    }

    public function banner(Request $request)
    {
        // dd($photoUrl);
        request()->user()->currentBusiness()->update([
            'banner' => $request->photo['url']
        ]);
        
        return redirect()->back()->with('success', 'Business banner updated');
    }

    public function edit(Photo $photo)
    {
        //
    }

    public function update(Request $request, Photo $photo)
    {
        //
    }

    public function destroy(Photo $photo)
    {
        Storage::delete($photo->path);
        
        $photo->delete();

        return redirect()->back()->with('success', 'Photo deleted!');
    }
}
