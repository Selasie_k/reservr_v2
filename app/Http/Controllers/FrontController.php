<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Category;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;

class FrontController extends Controller
{
    public function __invoke()
    {
        // request()->session()->put('success', 'success message');

        return Inertia::render('Front/Landing', [
            'packages' => Package::all(),
            'categories' => Category::all(),
            'businesses' => Business::online()
                        ->active()
                        ->online()
                        // ->whereNotNull('featured_at')
                        // ->orderBy('rating', 'desc')
                        ->orderBy('featured_at', 'desc')
                        ->limit(6)
                        ->get(['id','name','slug','banner','rating','reviews_count','description','category', 'logo', 'featured_at'])
        ]);
    }
}
