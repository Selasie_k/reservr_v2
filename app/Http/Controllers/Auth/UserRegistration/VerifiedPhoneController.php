<?php

namespace App\Http\Controllers\Auth\UserRegistration;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Models\PhoneVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class VerifiedPhoneController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(!$user = Session::get('registration')){
                return redirect()->route('registration-session.create');
            }
            $this->user = User::make($user);
            return $next($request);
        });
    }

    public function create(Request $request)
    {
        if(!$this->user->hasPendingVerificationCode()){
            $this->user->sendPhoneVerificationNotification();
        }

        return Inertia::render('Auth/Registration/VerifyPhone', [
            'user' => $this->user
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|exists:phone_verifications'
        ]);

        if(!$verification = PhoneVerification::firstWhere(['code' => $request->code, 'phone' => $this->user->phone])){
            throw ValidationException::withMessages(['code' => 'The provided code is invalid']);
        }

        $this->user->markPhoneAsVerified();

        $verification->delete();

        session(['registration' => $this->user]);

        return redirect()->route('registration-session.password.create');
    }

    public function resend()
    {
        // dd($this->user);
        $verification = PhoneVerification::firstWhere(['phone' => $this->user->phone]);
        $verification->delete();
        $this->user->sendPhoneVerificationNotification();

        return redirect()->back()->with('success', 'Verification code sent again');
    }
}
