<?php

namespace App\Http\Controllers\Auth\UserRegistration;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class RegistrationSessionController extends Controller
{

    public function create(Request $request)
    {
        if($request->invite_token){
            session(['invite_token' => $request->invite_token]);
        }
        
        return Inertia::render('Auth/Registration/Details', [
            'user' => $request->session()->get(
                    'registration', [ 
                        'first_name' => '', 
                        'last_name' => '', 
                        'email' => $request->email ?? '', 
                        'phone' => '',
                        'email_verified_at' => $request->invite_token ? now() : '',
                        'phone_verified_at' => '',
                    ]
                )
        ]);
    }

    public function store(Request $request)
    {
        session(['registration' => $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users',
                'phone' => 'required|unique:users',
                'email_verified_at' => 'nullable',
                'phone_verified_at' => 'nullable',
            ])
        ]);

        return redirect()->route('registration-session.verify-phone');
    }
}
