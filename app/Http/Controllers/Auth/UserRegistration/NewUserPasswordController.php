<?php

namespace App\Http\Controllers\Auth\UserRegistration;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class NewUserPasswordController extends Controller
{
    public function create()
    {
        return Inertia::render('Auth/Registration/Password');
    }

    public function store(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8|confirmed'
        ]);

        $user = $request->session()->get('registration');

        $user->password = $request->password;
        $user->save();
        
        Auth::login($user);

        if($token = session('invite_token')){
            return redirect()->route('users.teams.invitations.accept', ['token' => $token]);
        }

        return redirect()->route('users.dashboard');
    }
}
