<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PasswordUpdateController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
        ]);

        if(!Hash::check($request->old_password, $request->user()->password)){
            throw ValidationException::withMessages(['old_password' => 'The provided password is incorrect']);
        }

        auth()->user()->update($request->validate([
            'password' => 'required|min:8|confirmed',
        ]));

        return redirect()->route('users.profile')->with('success', 'Password updated');
    }
}
