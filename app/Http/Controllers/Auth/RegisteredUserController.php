<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;

class RegisteredUserController extends Controller
{

    public function details(Request $request)
    {
        return Inertia::render('Auth/Registration/Details', [
            'user' => $request->session()->get('registration', function() {
                        return User::make([ 'first_name' => '', 'last_name' => '', 'email' => '', 'phone' => '',]);
            })
        ]);
    }

    public function verifyPhone(Request $request)
    {
        $user = User::make($request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ]));

        session(['registration' => $user]);

        return Inertia::render('Auth/Registration/VerifyPhone', [
            'user' => $request->session()->get('registration')
        ]);
    }

    public function create()
    {
        return Inertia::render('Auth/Form', [
            'is_register' => true
        ]);
        // return Inertia::render('Auth/Register');
    }

    public function store(Request $request)
    {
        Auth::login(
            $user = User::create(
                $request->validate([
                    'first_name' => 'required|string|max:255',
                    'last_name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'phone' => 'required|string|max:10|unique:users',
                    'password' => 'required|string|confirmed|min:8',
                ])
            )
        );

        event(new Registered($user));

        return redirect()->intended(route('home'));
    }
}
