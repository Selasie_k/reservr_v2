<?php

namespace App\Http\Controllers\Partners\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Partners\Auth\BusinessOptionController;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class LoginController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return Inertia::render('Partners/Auth/Login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        if($token = $request->invite_token){
            return redirect()->route('partners.teams.invitations.accept', ['token' => $token]);
        }

        //this will not work if youre going for an intended route
        // if($request->user()->businesses()->count() > 1){
        //     return Inertia::render('Partners/Auth/ChooseBusiness', [
        //         //merge team businesses too
        //         'businesses' => auth()->user()->businesses()->select(['id', 'name', 'slug', 'logo'])->get()
        //     ]);
        // }

        return redirect()->intended(route('partners.dashboard'));
    }
}
