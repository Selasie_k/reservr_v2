<?php

namespace App\Http\Controllers\Partners\Auth;

use Inertia\Inertia;
use App\Models\Business;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessOptionController extends Controller
{
    public function __invoke()
    {
        return Inertia::render('Partners/Auth/ChooseBusiness', [
            //merge team businesses too
            'businesses' => auth()->user()->businesses()->select(['id', 'name', 'slug', 'logo'])->get()
        ]);
    }

    public function select(Business $business)
    {
        if(!$business->owner()->is(auth()->user())){
            abort(403);
        }

        auth()->user()->update(['current_business_id' => $business->id]);

        return redirect()->route('partners.dashboard');
    }
}
