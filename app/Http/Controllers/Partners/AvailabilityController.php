<?php

namespace App\Http\Controllers\Partners;

use Carbon\Carbon;
use Inertia\Inertia;
use App\Models\Business;
use App\Models\Availability;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AvailabilityController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Availability/Index', [
            'availability' => auth()->user()->currentBusiness()->availability,
        ]);
    }

    public function update(Request $request)
    {
        auth()->user()->currentBusiness()->availability->update($request->all());

        return redirect()->back()->with('success', 'Availability schedule updated!');
    }

    public function dateAvailability(Request $request, Business $business)
    {
        $date = Carbon::parse($request->date);
        $availability = $business->availability->getFullSchedule();
        $day = strtolower($date->format('l'));
        $bookingsOnDate = $business->reservations()->whereDate('appointment_date', $date->format('Y-m-d'))->get();

        $timeframes = collect($availability[$day]['time_instances'])
                        ->map(function($time) use ($bookingsOnDate, $business, $day){
                            return [
                                'time' => $time,
                                'booked' => $bookingsOnDate->filter(fn($booking) => strtotime($booking->appointment_time) == strtotime($time))->count(),
                                'capacity' => (int) $business->availability->$day['capacity'],
                                // 'remaining'
                            ];
                        });
        $services = $business->services->filter(fn($service) => in_array($day, $service->days_available));
        
        return response()->json([
            'date' => $date->format('Y-m-d'),
            'day_name' => $day,
            'is_available' => $availability[$day]['is_available'],
            'timeframes' => $timeframes,
            'services' => $services
        ]);
    }
}
