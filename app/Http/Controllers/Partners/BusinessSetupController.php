<?php

namespace App\Http\Controllers\Partners;

use Inertia\Inertia;
use App\Models\Package;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class BusinessSetupController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Setup/Index', [
            'user' => request()->user(),
            'packages' => Package::where('price', '>', 0)->get(),
        ]);
    }

    public function show(Request $request)
    {
        $business = auth()->user()->currentBusiness();

        return Inertia::render('Partners/Setup/Index', [
            'business' => $business,
            'categories' => Category::all()->pluck('name'),
        ]);
    }

    public function updateDetails(Request $request)
    {
        $business = auth()->user()->currentBusiness();

        $business->update($request->validate([
            'name' => ['required', Rule::unique('businesses')->ignore($business)],
            'email' => 'required|email',
            'phone' => 'required',
            'description' => 'required|max:1000|min:100',
            'category' => 'required',
        ]));

        return redirect()->back()->with('success', 'Business record updated!');
    }

    public function updateLocation(Request $request)
    {
        // dd($request->all());
        $business = auth()->user()->currentBusiness();

        $business->update($request->validate([
            'address' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'lat' => 'required|max:255',
            'lng' => 'required|max:255',
        ]));

        return redirect()->back()->with('success', 'Business record updated!');
    }

    public function updateSocialLinks(Request $request)
    {
        $business = auth()->user()->currentBusiness();

        $business->update($request->validate([
            'website' => 'nullable|max:255',
            'facebook' => 'nullable|max:255',
            'instagram' => 'nullable|max:255',
            'twitter' => 'nullable|max:255',
        ]));

        return redirect()->back()->with('success', 'Social links updated!');
    }
}
