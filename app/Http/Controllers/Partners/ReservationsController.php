<?php

namespace App\Http\Controllers\Partners;

use Inertia\Inertia;
use App\Models\Business;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Events\ReservationCreated;
use App\Http\Controllers\Controller;

class ReservationsController extends Controller
{
    public function index(Request $request)
    {
        $business = auth()->user()->currentBusiness();

        return Inertia::render('Partners/Reservations/Index', [
            'reservations' => $business->reservations()
                                ->filter($request->all())
                                ->paginate(10),
            'business' => $business,
            'services' => $business->services,
            'fullSchedule' => $business->availability->getFullSchedule(),
            'filters' => $request->all(),
        ]);
    }

    public function create()
    {
        return $this->edit();
    }

    public function edit(Reservation $reservation = null)
    {
        $business = auth()->user()->currentBusiness();
        
        return Inertia::render('Partners/Reservations/Form', [
            'reservation' => $reservation ?? (object) [],
            'services' => $business->services,
            'fullSchedule' => $business->availability->getFullSchedule(),
        ]);
    }

    public function store(Request $request)
    {
        $business = $request->user()->currentBusiness();

        $reservation = $business->reservations()
            ->create($request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'email' => 'nullable',
                // 'count' => 'required',
                'appointment_date' => 'required',
                'appointment_time' => 'required',
                'initiated_by' => 'required',
                'services' => 'nullable|array'
            ]));

            $reservation->update(['tenant_id' => $business->user_id]);

            $reservation->confirm();

            event(new ReservationCreated($reservation));

            return redirect()->route('partners.reservations')->with('success', 'Reservation successfully created!');
    }

    public function show(Reservation $reservation)
    {
        if(!$reservation->seen){
            $reservation->markAsSeen();
        }

        return Inertia::render('Partners/Reservations/Show', [
            'reservation' => $reservation,
        ]);
    }
}
