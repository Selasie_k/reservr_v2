<?php

namespace App\Http\Controllers\Partners;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationStatusContontroller extends Controller
{
    public function update(Request $request, Reservation $reservation)
    {
        $request->validate([
            'action' => 'required|in:confirm,decline,cancel'
        ]);

        $action = $request->action;

        $response = $reservation->$action();

        return redirect()->back()->with($response['status'], $response['message']);
    }
}
