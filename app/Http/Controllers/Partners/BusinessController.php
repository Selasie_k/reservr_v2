<?php

namespace App\Http\Controllers\Partners;

use Inertia\Inertia;
use App\Models\Business;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BusinessController extends Controller
{
    public function index()
    {
        // if(auth()->user()->currentTeam->businesses()->count() == 1){
        //     return $this->show(auth()->user()->currentTeam->businesses()->first(), request());
        // }

        return Inertia::render('Partners/Businesses/Index', [
            // 'businesses' => auth()->user()->businesses,
            'businesses' => auth()->user()->isOnOwnTeam() 
                ? auth()->user()->businesses
                : auth()->user()->teamBusinesses()->get()
        ]);
    }

    public function create()
    {
        return $this->edit(new Business);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        // dd($request->all());

        $business = $user->businesses()->create($request->validate([
            'name' => 'required|unique:businesses',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users',
            'description' => 'required|max:1000|min:50',
            'category' => 'required',

            'address' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
            'lat' => 'required|max:255',
            'lng' => 'required|max:255',

            'website' => 'nullable|max:255',
            'facebook' => 'nullable|max:255',
            'instagram' => 'nullable|max:255',
            'twitter' => 'nullable|max:255',
        ]));

        $user->update(['current_business_id' => $business->id]);

        return redirect()
                ->route('partners.profile')
                ->with('success', 'Business created. Proceed to complete your setup');
    }

    public function show(Business $business, Request $request)
    {
        return Inertia::render('Partners/Businesses/Show', [
            'business' => $business->load('services'),
            'services' => $business->services,
            'businesses' => auth()->user()->businesses()->select(['id', 'name', 'slug'])->get(),
            'availability' => $business->availability,
            'fullSchedule' => $business->availability->getFullSchedule(),
            'reservations' => $business->reservations()
                                ->latest()
                                ->filter($request->all())
                                ->when($request->date ?? null, function($query, $date){
                                        $query->whereDate('appointment_date', $date);
                                })->paginate(10),
        ]);
    }

    public function edit(Business $business)
    {
        return Inertia::render('Partners/Businesses/Form', [
            'business' => $business->exists ? $business : (object) [],
            'categories' => Category::all()->pluck('name'),
        ]);
    }

    public function destroy(Business $business)
    {
        if($business->purge()){
            auth()->user()->update([ 'current_business_id' => null ]);
        }
        return redirect('/');
    }
}
