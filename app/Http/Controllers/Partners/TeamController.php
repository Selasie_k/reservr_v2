<?php

namespace App\Http\Controllers\Partners;

use App\Models\Team;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Mpociot\Teamwork\Exceptions\UserNotInTeamException;

class TeamController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Team/Index', [
            'teams' => request()->user()->teams()->with('owner:id')->withCount('users')->get(), // list of all team user is a member of
            'ownedTeam' => request()->user()->ownedTeams()->first(), // users own team
            'invites' => request()->user()->invites->load('team:id,name,display_name'), // invitations sent to this user
            'currentTeam' => request()->user()->currentTeam, // current team the user is on
        ]);
    }

    public function show(Team $team)
    {
        return Inertia::render('Partners/Team/Show', [
            'team' => $team->load('users:id,first_name,last_name,email,team_user.events,team_user.businesses', 'owner:id,first_name,last_name,email', 'invites'),
            'currentTeam' => request()->user()->currentTeam,
            'events' => request()->user()->events->transform(fn($event) => ['id' => $event->id, 'name' => $event->name]),
            'businesses' => request()->user()->businesses->transform(fn($business) => ['id' => $business->id, 'name' => $business->name]),
        ]);
    }

    public function switch(Team $team)
    {
        try {
            auth()->user()->switchTeam($team);
        } catch (UserNotInTeamException $e) {
            return redirect()->back()->with('error', "You're not a member of this team");
        }

        return Inertia::location(route('partners.teams')); // force full page reload
        // return redirect()->back()->with('success', "You've successfully switched your current team");
    }

    // public function leave(Team $team)
    // {
    //     $user = Auth::user();

    //     $this->removeUserFromTeam($user, $team);

    //     return Inertia::location(route('partners.teams')); // force full page reload
    // }

    // public function removeMember(Team $team, User $user)
    // {
    //     if(! $team->owner->is(auth()->user())){
    //         return redirect()->back()->with('error', "You're not the owner of this team");
    //     }

    //     $this->removeUserFromTeam($user, $team);

    //     return redirect()->back()->with('success', "{$user->name} has been removed from this team");
    // }

    // private function removeUserFromTeam($user, $team)
    // {
    //     $teamUserModel = $user->teams()->where('team_id', $team->id)->first()->pivot;
    //     // $events =  $teamUserModel->events;
    //     // $businesses =  $teamUserModel->businesses;

    //     $user->teamEvents()->detach(json_decode($teamUserModel->events));
    //     $user->teamBusinesses()->detach(json_decode($teamUserModel->businesses));
    //     $user->detachTeam($team);
    //     $user->switchTeam($user->ownedTeam());
    // }
}
