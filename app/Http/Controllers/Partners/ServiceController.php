<?php

namespace App\Http\Controllers\Partners;

use App\Models\Business;
use Illuminate\Http\Request;
use App\Models\BusinessService;
use App\Http\Controllers\Controller;
use Inertia\Inertia;

class ServiceController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Services/Index', [
            'services' => auth()->user()->currentBusiness()->services
        ]);
    }

    public function store(Request $request)
    {
        $request->user()
            ->currentBusiness()
            ->services()
            ->create($request->validate([
                'name' => 'required|string|max:255',
                'charge' => 'required|numeric',
                'days_available' => 'nullable|array',
            ]));

        return redirect()->back()->with('success', 'Service added successfully!');
    }

    public function update(Request $request, BusinessService $service)
    {
        $service->update($request->validate([
            'name' => 'required|string|max:255',
            'charge' => 'required|numeric',
            'days_available' => 'nullable|array',
        ]));

        return redirect()->back()->with('success', 'Service updated successfully!');
    }

    public function destroy(BusinessService $service)
    {
        $service->delete();

        return redirect()->back()->with('success', 'Service deleted!');
    }
}
