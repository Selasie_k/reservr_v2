<?php

namespace App\Http\Controllers\Partners;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublishedEventController extends Controller
{
    public function store(Event $event)
    {
        if($event->isExpired()){
            return redirect()->back()->with('error', 'Cannot publish expired event');
        }

        $event->publish();
        return redirect()->back()->with('success', 'Event is now live');
    }

    public function destroy(Event $event)
    {
        $event->unpublish();
        return redirect()->back()->with('success', 'Event has been unpublished');
    }
}
