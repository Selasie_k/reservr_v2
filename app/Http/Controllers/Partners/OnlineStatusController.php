<?php

namespace App\Http\Controllers\Partners;

use App\Models\Business;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OnlineStatusController extends Controller
{
    public function update()
    {
        $business = auth()->user()->currentBusiness();

        if($business->is_online){
            $business->goOffline();
            return redirect()->back()->with('success', 'This business has been taken offline');
        }

        if($business->hasInsufficientBalance()){
            return redirect()->back()->with('error', 'Sorry! Your balance is insufficient, please top-up your account');
        }

        if($business->availability->availableDays()->count() < 1){
            return redirect()
                ->route('partners.availability')
                ->with('error', 'Please set your availability before going online');
        }

        $business->goOnline();
        return redirect()->back()->with('success', 'This business is now visible on the Reservr platform');
    }
}
