<?php

namespace App\Http\Controllers\Partners;

use App\Models\Team;
use App\Models\User;
use App\Models\TeamInvite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Application;
use Mpociot\Teamwork\Events\UserInvitedToTeam;
use Mpociot\Teamwork\Facades\Teamwork;
use Mpociot\Teamwork\Teamwork as TeamworkTeamwork;

class TeamInviteController extends Controller
{
    public function store(Request $request, Team $team)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $user = User::whereEmail($request->email)->first();

        if($user && $team->hasUser($user)){
            return redirect()->back()->with('error', 'User already part of this team');
        }

        if(Teamwork::hasPendingInvite($request->email, $team)){
            return redirect()->back()->with('error', 'User already invited to your team');
        }

        Teamwork::inviteToTeam($request->email, $team, function($invite){
            $invite->update([
                'events' => request()->events,
                'businesses' => request()->businesses,
            ]);
        });

        return redirect()->back()->with('success', 'Invitation sent!');
    }

    public function resend(TeamInvite $invite)
    {
        $invite->update(['created_at' => now()]);
        
        event(new UserInvitedToTeam($invite));

        return redirect()->back()->with('success', 'Invite sent again!');
    }

    public function accept($token)
    {
        $invite = Teamwork::getInviteFromAcceptToken($token);

        if (! $invite) {
            return redirect()->back()->with('error', 'This invitation is invalid');
        }

        Teamwork::acceptInvite($invite);

        // $invite->user->teamEvents()->syncWithoutDetaching(json_decode($invite->events));
        // $invite->user->teamBusinesses()->syncWithoutDetaching(json_decode($invite->businesses));

        $invite->user->associateTeamEventsWithUser($invite->events, $invite->team);
        $invite->user->associateTeamBusinessesWithUser($invite->businesses, $invite->team);

        // $invite->user->teamEvents()->syncWithoutDetaching( 
        //     $this->appendTeamIdToPivotTable(json_decode($invite->events), $invite->team->id) 
        // );

        // $invite->user->teamBusinesses()->syncWithoutDetaching( 
        //     $this->appendTeamIdToPivotTable(json_decode($invite->businesses), $invite->team->id)
        // );

        // $invite
        //     ->user
        //     ->teams()
        //     ->where('team_id', $invite->team->id)
        //     ->first()
        //     ->pivot
        //     ->update([
        //         'events' => $invite->events,
        //         'businesses' => $invite->businesses,
        //     ]);

        return redirect()->back()->with('success', "You're now part of {$invite->team->display_name}");
    }

    public function decline($token)
    {
        $invite = Teamwork::getInviteFromDenyToken($token);

        if (! $invite) {
            return redirect()->back()->with('error', 'This invitation is invalid');
        }

        $invite->delete();

        return redirect()->back()->with('success', "Invitation declined!");
    }

    public function destroy(TeamInvite $invite)
    {
        $invite->delete();

        return redirect()->back()->with('success', 'Invite deleted!');
    }

    // private function appendTeamIdToPivotTable(array $modelIDs, int $team_id)
    // {
    //     return collect($modelIDs)->map(function($item) use ($team_id) {
    //         return ['id' => $item, 'value' => ['team_id' => $team_id]];
    //     })->keyBy('id')->map(fn($item) => $item['value']);
    // }
}
