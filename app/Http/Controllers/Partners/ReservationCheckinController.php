<?php

namespace App\Http\Controllers\Partners;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationCheckinController extends Controller
{
    public function store(Reservation $reservation)
    {
        $reservation->checkIn();

        return redirect()->back()->with('success', 'User checked in successfully');
    }
}
