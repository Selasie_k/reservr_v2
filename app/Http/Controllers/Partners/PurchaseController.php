<?php

namespace App\Http\Controllers\Partners;

use Inertia\Inertia;
use App\Models\Package;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchaseController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Purchases/Index', [
            'packages' => Package::all(),
            'purchases' => auth()->user()->purchases()->with('purchasable')->latest()->paginate(20),
        ]);
    }

    public function store(Package $package)
    {
        $payment_link = auth()->user()->purchase($package);
        return Inertia::location($payment_link);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }
}
