<?php

namespace App\Http\Controllers\Partners;

use App\Models\Reservation;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Timeslot;

class _ReservationsController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Partners/Reservations/Index', [
            'reservations' => auth()->user()->reservations()->latest()->with('reservable')->paginate(15),
        ]);
    }

    public function create()
    {
        return $this->edit(new Reservation);
    }

    public function show(Reservation $reservation)
    {
        return Inertia::render('Partners/Reservations/Show', [
            'reservation' => $reservation->load('reservable:id,name'),
        ]);
    }

    public function edit(Reservation $reservation)
    {
        return Inertia::render('Partners/Reservations/Form', [
            'reservation' => $reservation->exists ? $reservation : (object) [],
            // 'timeslot' => $slot ? $slot->load('slotable') : $reservation->load('slot:id,start_tim,end_time,slotable')
        ]);
    }

    // public function destroy(Reservation $reservation)
    // {
    //     $reservation->delete();
    //     return redirect()->route('partners.reservations')->with('success', 'Reservation deleted!');
    // }
    // public function update(Request $request, Reservation $reservation)
    // {
    //     $reservation->update($request->validate([
    //             // 'first_name' => 'required',
    //             // 'last_name' => 'required',
    //             // 'phone' => 'required',
    //             // 'email' => 'required',
    //             // 'count' => 'required',
    //             'appointment_date' => 'required',
    //             'appointment_time' => 'required',
    //         ]));
        
    //     return redirect()->back()->with('success', 'Reservation successfully updated!');
    // }
}
