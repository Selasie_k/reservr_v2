<?php

namespace App\Http\Controllers\Partners;

use App\Models\Team;
use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TeamMemberController extends Controller
{
    public function update(Request $request, Team $team, User $user)
    {

        $user->associateTeamEventsWithUser($request->events, $team);
        $user->associateTeamBusinessesWithUser($request->businesses, $team);

        return redirect()->back()->with('success', 'Team member updated!');
    }
    
    public function destroy(Team $team, User $user = null)
    {
        if(!$user){ 
            $user = Auth::user();
            $this->removeUserFromTeam($user, $team);
            return Inertia::location(route('partners.teams')); // force full page reload
        } 
        
        // for when this action is initiated by team owner
        if(! $team->owner->is(auth()->user())){
            return redirect()->back()->with('error', "You're not the owner of this team");
        }
        
        $this->removeUserFromTeam($user, $team);
        return redirect()->back()->with('success', "{$user->name} has been removed from this team");     
    }

    private function removeUserFromTeam($user, $team)
    {
        $user->teamEvents($team)->detach();

        $user->teamBusinesses($team)->detach();

        $user->detachTeam($team);

        $user->switchTeam($user->ownedTeam());
    }
}
