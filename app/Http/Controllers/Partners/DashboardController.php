<?php

namespace App\Http\Controllers\Partners;

use Carbon\Carbon;
use Inertia\Inertia;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __invoke(Request $request){
        
        $business = $request->user()->currentBusiness();

        return Inertia::render('Partners/Dashboard/Index', [
            'reservations_today' => $business->reservations()
                    ->whereDate('created_at', Carbon::today())
                    ->limit(5)
                    ->get(['id', 'first_name', 'last_name', 'phone', 'appointment_time', 'appointment_date']),
            'reservations_new' => $business->reservations()
                    ->whereDate('created_at', Carbon::today())
                    ->latest()
                    ->limit(5)
                    ->get(['id', 'first_name', 'last_name', 'phone', 'appointment_time', 'appointment_date', 'created_at']),
            'reviews' => []
            // 'stats' => [
            //     'balance' => auth()->user()->total_balance,
            //     'reservations' => auth()->user()->reservations()->count(),
            //     'events' => auth()->user()->events()->count()
            // ],
        ]);
    }
}
