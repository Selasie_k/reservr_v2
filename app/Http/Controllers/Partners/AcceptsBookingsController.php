<?php

namespace App\Http\Controllers\Partners;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcceptsBookingsController extends Controller
{
    public function update()
    {
        $business = Auth::user()->currentBusiness();

        if($business->accepting_bookings){
            $business->update(['accepting_bookings' => 0]);
            return redirect()->back()->with('success', 'Bookings feature disabled');
        }
        $business->update(['accepting_bookings' => 1]);
        return redirect()->back()->with('success', 'Bookings feature enabled');
    }
}
