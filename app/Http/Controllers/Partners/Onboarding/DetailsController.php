<?php

namespace App\Http\Controllers\Partners\Onboarding;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Business;
use Illuminate\Support\Facades\Session;

class DetailsController extends Controller
{
    public function create()
    {
        return Inertia::render('Partners/Auth/Registration', [
            'details' => ''
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:255|unique:businesses',
            'email' => 'required|max:255|email',
            'phone' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'country' => 'required|max:255',
            'website' => 'nullable|max:255',
            'lat' => 'required|max:255',
            'lng' => 'required|max:255',
            'personal_email' => 'required|max:255',
        ]);

        Session::put('partner-onboarding-details', $data);

        $user = User::whereEmail($data['personal_email'])->first();

        return redirect()->route('partners.onboarding.account.create', [
            'user' => $user
        ]);
    }
}
