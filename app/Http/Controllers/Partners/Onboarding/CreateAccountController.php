<?php

namespace App\Http\Controllers\Partners\Onboarding;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Business;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CreateAccountController extends Controller
{
    public function create(User $user = null)
    {
        if(!Session::has('partner-onboarding-details')){
            return redirect()->route('partners.onboarding.create');
        }
        return Inertia::render('Partners/Auth/CreateAccount', [
            'user' => $user,
            'email' => session()->get('partner-onboarding-details')['personal_email'],
        ]);
    }

    public function store(Request $request)
    {
        if(!Session::has('partner-onboarding-details')){
            return redirect()->route('partners.onboarding.create');
        }

        if($request->boolean('has_account')) {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return back()->with('error', 'Your credentials don\'t match. Please try again');
            }

        } else {
            $data = $request->validate([
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|max:255|email',
                'phone' => 'required|max:255',
                'password' => 'required|confirmed|min:8|max:255',
            ]);

            //create user record
            //log new user in
            Auth::login(User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'password' => Hash::make($data['password']) ,
            ]));
        }

        $data = collect(Session::get('partner-onboarding-details'))
                ->forget('personal_email');

        $business = Business::make($data->toArray());

        $business->user_id = auth()->user()->id;

        $business->save();

        return redirect()->route('partners.dashboard')
                    ->with('success', 'Account created!');
    }
}

    // private function authenticate($credentials){
    //     return Auth::attempt($credentials);
    // }

