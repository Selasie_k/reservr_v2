<?php

namespace App\Http\Controllers\Partners;

use App\Models\Event;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    public function index()
    {
        return Inertia::render('Partners/Events/Index', [
            // 'events' => auth()->user()->currentTeam->events()->orderBy('date', 'desc')->withCount('reservations')->paginate(10),
            'events' => auth()->user()->isOnOwnTeam() 
                ? auth()->user()->events()->orderBy('date', 'desc')->withCount('reservations')->paginate(10)
                : auth()->user()->teamEvents()->orderBy('date', 'desc')->withCount('reservations')->paginate(10),
            // 'events' => auth()->user()->teamEvents()->orderBy('date', 'desc')->withCount('reservations')->paginate(10),
        ]);
    }

    public function create()
    {
        return $this->edit(new Event);
    }

    public function store(Request $request)
    {
        $event = auth()->user()->currentTeam->events()->create($request->validate([
            'name' => 'required|string:255',
            'description' => 'required|string:1000',
            'date' => 'required|date',
            'time' => 'required',
            'location' => 'required|string:255',
            'capacity' => 'required|numeric',
            'price' => 'required|numeric',
            'poster' => 'required|file|max:1500',
            'start_accepting_at' => 'required',
            'stop_accepting_at' => 'required',
            'must_confirm' => 'required',
            'limit_per_user' => 'required',
            'tenant_id' => 'required',
        ]));

        // return redirect()
        //         ->route('partners.events.show', ['event' => $event])
        //         ->with('success', 'Event created');
        return $this->show($event);
    }

    public function show(Event $event)
    {
        return Inertia::render('Partners/Events/Show', [
            'event' => $event,
            // 'reservations' => $event->reservations()->with('payment')->get(),
            'reservations' => $event->reservations()
                        ->with('payment')
                        ->latest()
                        ->filter(request()->all())
                        ->paginate(10),
            'total_bookings' => $event->reservations()->count(),
        ]);
    }

    public function edit(Event $event)
    {
        return Inertia::render('Partners/Events/Form', [
            'event' => $event->exists ? $event : (object) [],
        ]);
    }

    public function update(Request $request, Event $event)
    {
        $event->update($request->validate([
            'name' => 'required|string:255',
            'description' => 'required|string:1000',
            'date' => 'required|date',
            'time' => 'required',
            'location' => 'required|string:255',
            'capacity' => 'required|numeric',
            'price' => 'required|numeric',
            'poster' => 'nullable|file|max:1500',
            'start_accepting_at' => 'required',
            'stop_accepting_at' => 'required',
            'must_confirm' => 'required',
            'limit_per_user' => 'required',
            'tenant_id' => 'required',
        ]));

        // if($request->filled('photo')){

        // }

        return redirect()->route('partners.events.show', ['event' => $event])->with('success', 'Event updated!');
    }

    public function destroy(Event $event)
    {
        //
    }
}
