<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class LegalController extends Controller
{
    public function privacy()
    {
        return Inertia::render('Front/Legal/Privacy');
    }

    public function terms()
    {
        return Inertia::render('Front/Legal/Terms');
    }
}
