<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Event;
use App\Models\Client;
use App\Models\Business;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $events = Event::allTeams()->map(function($item, $key){
            return [
                'name' => $item->name,
                'description' => $item->description,
                'path' => $item->path,
                'date' => $item->date->toFormattedDateString(),
                'type' => 'event',
                'location' => $item->location,
                'buttonText' => 'Book a seat',
            ];
        });
        
        $businesses = Business::allTeams()->map(function($item, $key){
            return [
                'name' => $item->name,
                'description' => $item->category,
                'path' => $item->path,
                'date' => $item->email,
                'type' => 'business',
                'location' => $item->location,
                'buttonText' => 'Make reservation',
            ];
        });

        // $results = $events->merge($businesses)->sortBy('name')->values();
        $results = collect([$businesses, $events])->flatten(1)->sortBy('name')->values();

        // dd();

        return Inertia::render('Front/ReservationPages/SearchResults', [
            'results' => $results,
            'search_term' => $request->term
        ]);
    }
    
}
