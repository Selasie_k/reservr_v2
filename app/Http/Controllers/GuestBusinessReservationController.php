<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Business;
use Illuminate\Http\Request;
use App\Events\ReservationCreated;

class GuestBusinessReservationController extends Controller
{
    public function store(Request $request, Business $business)
    {
        if(!$business->tenant->hasSufficientBalance()){
            return redirect()->back()->with('error', 'Balance too low to fulfill this reservation');
        }

        $reservation = $business
                ->reservations()
                ->create($request->validate([
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required|phone:GH',
                    'email' => 'required|email',
                    'count' => 'required|numeric|min:1',
                    'appointment_date' => 'required',
                    'appointment_time' => 'required',
                    'initiated_by'=> 'boolean',
                    'must_purchase'=> 'boolean',
                    'services' => 'array',
                    'amount' => 'numeric',
                ]));

        $reservation->update([ 'tenant_id' => $business->tenant->id ]);

        // if($reservation->initiated_by){
        //     $reservation->tenant->debit();

        //     event(new ReservationCreated($reservation));

        //     return redirect()->back()->with('success', 'Reservation successfully created!');
        // }

        if($reservation->amountToPay() > 0){
            $paymentLink = $reservation->getPaymentLinkFor(
                    $reservation->reservable_type, // Event, Business
                    $reservation->reservable_id
                );
            return Inertia::location($paymentLink);
        }

        $reservation->tenant->debit();

        event(new ReservationCreated($reservation));

        return redirect()->route('reservations.confirmation', [
            'reservation' => $reservation
        ]);
    }
}
