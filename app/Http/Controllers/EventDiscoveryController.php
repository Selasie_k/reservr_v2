<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Event;
use Illuminate\Http\Request;

class EventDiscoveryController extends Controller
{
    public function index(Request $request)
    {
        // return Inertia::render('Front/ReservationPages/Events/Index', [
        return Inertia::render('Front/EventDiscovery/Index', [
            'results' => Event::allTeams()->published()->active()->search($request->only('name'))->orderBy('date', 'asc')->get()
        ]);
    }

    public function show(Event $event)
    {
        return Inertia::render('Front/EventDiscovery/Show', [
            'event' => $event
        ]);
    }
}
