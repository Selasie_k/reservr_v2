<?php

namespace App\Http\Controllers\Users;

use Inertia\Inertia;
use App\Models\Business;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Events\ReservationCreated;
use App\Http\Controllers\Controller;

class BookingsController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Users/Bookings/Index', [
            'bookings' => $request->user()->bookings()->with('business:id,name,slug,logo')->orderBy('appointment_date', 'desc')->get(),
        ]);
    }

    public function store(Request $request, Business $business)
    {
        // dd($request->all());
        $data = $request->validate([
            // 'user_id' => 'required',
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            // 'phone' => 'required',
            // 'email' => 'required|email',
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'is_client_originated'=> 'boolean',
            'must_purchase'=> 'boolean',
            'services' => 'array',
            'amount' => 'numeric',
            'business_id' => 'required',
            'tenant_id' => 'required',
        ]);

        $user = auth()->user();

        $data = array_merge($data, [
            // 'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'phone' => $user->phone,
            'email' => $user->email
        ]);

        // dd($data);

        // $reservation = $business->reservations()->create($data);
        $reservation = $user->bookings()->create($data);

        // dd($reservation);

        // $reservation->update([ 'tenant_id' => $business->tenant->id ]);

        if($reservation->amountToPay() > 0){
            $paymentLink = $reservation->getPaymentLink();
            return Inertia::location($paymentLink);
        }

        $reservation->business->tenant->debit();

        event(new ReservationCreated($reservation));

        // return redirect()->route('users.bookings.confirmation', [
        //     'reservation' => $reservation
        // ]);

        return redirect()->route('users.bookings.show', [
            'booking' => $reservation
        ])->with('success', 'Your reservation has been created successfully');
    }

    public function show(Reservation $booking)
    {
        if($booking->user_id !== auth()->id()){
            abort(403);
        }

        return Inertia::render('Users/Bookings/Show', [
            'booking' => $booking->load('business:id,name,slug,logo,rating,reviews_count')
        ]);
    }

    public function update(Request $request, Reservation $reservation)
    {
        //
    }

    public function confirmation(Reservation $reservation)
    {
        return Inertia::render('Front/ReservationPages/Confirmation', [
            'reservation' => [
                'first_name' => $reservation->user->first_name,
                'title' => $reservation->business->name,
                'code' => $reservation->code,
                'date' => $reservation->appointment_date,
                'time' => $reservation->appointment_time,
                'seats' => $reservation->count,
            ]
        ]);
    }

    public function destroy(Reservation $reservation)
    {
        //
    }
}
