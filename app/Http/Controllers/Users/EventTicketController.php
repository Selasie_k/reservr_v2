<?php

namespace App\Http\Controllers\Users;

use Inertia\Inertia;
use App\Models\Event;
use App\Models\EventTicket;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Events\ReservationCreated;
use App\Http\Controllers\Controller;

class EventTicketController extends Controller
{
    public function index(Request $request)
    {
        return Inertia::render('Users/Tickets/Index', [
            'tickets' => $request->user()->tickets()->with('reservable:id,name,code,date,time,poster')->orderBy('appointment_date', 'desc')->get()
        ]);
    }

    public function store(Request $request, Event $event)
    {
        $reservation = $event->reservations()
            ->create($request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'count' => 'required',
                'appointment_date' => 'required',
                'appointment_time' => 'required',
                'is_client_originated' => 'boolean'
            ]));

            event(new ReservationCreated($reservation));

            return redirect()->back()->with('success', 'Ticket created successfully!');
    }

    public function show(Reservation $ticket)
    {
        if($ticket->user_id !== auth()->id()){
            abort(403);
        }

        return Inertia::render('Users/Tickets/Show', [
            'ticket' => $ticket->load('reservable:id,name,code,location,poster','payment')
        ]);
    }
}
