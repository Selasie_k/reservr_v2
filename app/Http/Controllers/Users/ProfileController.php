<?php

namespace App\Http\Controllers\Users;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function edit(Request $request)
    {
        return Inertia::render('Users/Profile/Edit', [
            'user' => $request->user(),
            'packages' => Package::all()
        ]);
    }

    public function update(Request $request)
    {

        $user = $request->user();

        $input = $request->validate([
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => [ 'required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                'photo' => ['nullable', 'file', 'mimes:jpg,jpeg,png', 'max:1024'],
            ]);

        if (isset($input['photo'])) {
            $user->updateProfilePhoto($input['photo']);
        }

        if ($input['email'] !== $user->email && $user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser($user, $input);
        } else {
            $user->forceFill([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'email' => $input['email'],
            ])->save();
        }

        return redirect()->back()->with('success', 'Profile information updated!');
    }

    protected function updateVerifiedUser($user, array $input)
    {
        $user->forceFill([
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'email_verified_at' => null,
        ])->save();

        $user->sendEmailVerificationNotification();
    }

    public function destroy(Request $request)
    {
        if(auth()->user()->isPartner()){
            return redirect()->back()->with('error', 'Unable to delete user with businesses attached');
        }

        $request->user()->delete();
        
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
