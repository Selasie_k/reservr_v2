<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use App\Contracts\SmsGatewayInterface;
use App\Contracts\SMSServiceContract;

class MNotifyService implements SMSServiceContract
{
    protected $endPoint; 

    protected $apiKey;

    protected $url;

    protected $from;

    protected $to;

    protected $message;


    public function __construct()
    {
        $this->apiKey = config('mnotify.key');
        
        $this->endPoint = config('mnotify.uri');

        $this->url = $this->endPoint . '?key=' . $this->apiKey;
    }

    public function from($from)
    {
        $this->from = $from;

        return $this;
    }

    public function to($to)
    {
        if(is_string($to)){
            $to = explode(',', $to);
        }
        $this->to = $to;

        return $this;
    }

    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    public function send()
    {
        $response = Http::post($this->url, [
            'recipient' => $this->to,
            'sender' => $this->from,
            'message' => $this->message,
            'is_schedule' => 'false',
            'schedule_date' => ''
        ]);

        return $response->body();
    }
}