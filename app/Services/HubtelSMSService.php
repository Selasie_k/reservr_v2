<?php

namespace App\Services;

use App\Contracts\SMSServiceContract;
use Illuminate\Support\Facades\Http;

class HubtelSMSService implements SMSServiceContract
{
    protected $endPoint; 

    protected $client_id;

    protected $client_secret;

    protected $from;

    protected $to;

    protected $message;


    public function __construct()
    {
        $this->client_id = config('hubtel.client_id');
        $this->client_secret = config('hubtel.client_secret');
        $this->endPoint = config('hubtel.endpoint');
    }

    public function from($from)
    {
        $this->from = $from;

        return $this;
    }

    public function to($to)
    {
        $this->to = $to;

        return $this;
    }

    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    public function send()
    {
        return Http::withBasicAuth($this->client_id, $this->client_secret)
                ->get($this->endPoint, [
                    'clientid' => $this->client_id,
                    'clientsecret' => $this->client_secret,
                    'from' => $this->from,
                    'to' => $this->to,
                    'content' => $this->message
                ])->body();
    }
}