<?php

namespace App\Services;

use App\Contracts\PaymentGateway;
use Illuminate\Support\Facades\Http;

class Paystack implements PaymentGateway
{
    public static function paymentLink(array $data)
    {
        $paymentEndpoint = "https://api.paystack.co/transaction/initialize";

        return Http::withToken(config('paystack.secret_key'))
            ->post($paymentEndpoint, [
                'email' => $data['email'] ?? 'hello@reservr.cc',
                'amount' => $data['amount'] * 100, // cedis to pessewas
                'currency' => 'GHS',
                'reference' => $data['ref'],
                'channels' => ['card', 'bank', 'ussd', 'qr', 'mobile_money', 'bank_transfer'],
                'callback_url' => route('paystack-payment-redirect'),
            ])->json()['data']['authorization_url'];
    }
}
