<?php

namespace App\Services;

use App\Contracts\PaymentGateway;
use Illuminate\Support\Facades\Http;

class Flutterwave implements PaymentGateway
{
    public static function paymentLink(array $data)
    {
        $paymentEndpoint = "https://api.flutterwave.com/v3/payments";

        return Http::withToken(config('flutterwave.secret_key'))
            ->post($paymentEndpoint, [
                'public_key' => config('flutterwave.public_key'),
                'tx_ref' => $data['ref'],
                'amount' => $data['amount'],
                'currency' => 'GHS',
                'payment_options' => 'mobilemoneyghana, card, ussd',
                'redirect_url' => route('flutterwave-payment-redirect'),
                'customer' => [
                    'name' => $data['name'],
                    'email' => $data['email'] ?? 'hello@reservr.cc',
                    'phonenumber' => $data['phone'],
                ],
                'customizations' => [
                    'title' => 'Reservr.cc',
                    'description' => $data['description']
                ]
            ])->json()['data']['link'];
    }
    
    // public static function generatePaymentLinkFor(Purchase $purchase)
    // {
    //     $paymentEndpoint = "https://api.flutterwave.com/v3/payments";

    //     return Http::withToken(config('flutterwave.secret_key'))
    //         ->post($paymentEndpoint, [
    //             'public_key' => config('flutterwave.public_key'),
    //             'tx_ref' => $purchase->ref,
    //             'amount' => $purchase->amount,
    //             'currency' => 'GHS',
    //             'payment_options' => 'mobilemoneyghana, card, ussd',
    //             'redirect_url' => route('payment-redirect'),
    //             'customer' => [
    //                 'name' => $purchase->user->name,
    //                 'email' => $purchase->user->email ?? 'hello@reservr.com',
    //                 'phonenumber' => $purchase->user->phone,
    //             ],
    //             'customizations' => [
    //                 'title' => 'Reservr.cc',
    //                 'description' => "Payment for {$purchase->bookings}s on reservr.cc"
    //             ]
    //         ]);
    // }

    // public static function verify($trasaction_id) : PaymentModel
    // {
    //     $endpoint = "https://api.flutterwave.com/v3/transactions/{$trasaction_id}/verify";

    //     return new PaymentModel(
    //         Http::withToken(config('flutterwave.secret_key'))->get($endpoint)->json()
    //     );
    // }
}
