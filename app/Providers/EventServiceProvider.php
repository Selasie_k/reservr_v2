<?php

namespace App\Providers;

use App\Events\ReservationCancelled;
use App\Events\ReservationConfirmed;
use App\Events\ReservationCreated;
use App\Listeners\SendInviteNotification;
use App\Listeners\SendNewReservationNotifications;
use App\Listeners\SendNewUserRegisteredNotification;
use App\Listeners\SendReservationCancelledNotifications;
use App\Listeners\SendReservationConfirmationNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Mpociot\Teamwork\Events\UserInvitedToTeam;
use Mpociot\Teamwork\Events\UserLeftTeam;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendNewUserRegisteredNotification::class,
        ],
        ReservationCreated::class => [
            SendNewReservationNotifications::class
        ],
        ReservationConfirmed::class => [
            SendReservationConfirmationNotification::class
        ],
        ReservationCancelled::class => [
            SendReservationCancelledNotifications::class
        ],

        UserInvitedToTeam::class => [
            SendInviteNotification::class
        ],
        UserLeftTeam::class => [

        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
