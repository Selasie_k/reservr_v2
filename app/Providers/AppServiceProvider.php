<?php

namespace App\Providers;

use App\Contracts\PaymentGateway;
use App\Contracts\SMSServiceContract;
use App\Notifications\Channels\HubtelSMSChannel;
use App\Services\MNotifyService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Channels\MnotifyChannel;
use App\Notifications\Channels\SMSChannel;
use App\Services\Flutterwave;
use App\Services\HubtelSMSService;
use App\Services\Paystack;
use NotificationChannels\Telegram\TelegramChannel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Notification::extend('sms', function ($app) {
            return new SMSChannel(new HubtelSMSService);
            // return new SMSChannel(new MNotifyService);
        });

        Notification::extend('telegram', function ($app) {
            return TelegramChannel::class;
        });

        $this->app->bind(PaymentGateway::class, Paystack::class);
        // $this->app->bind(PaymentGateway::class, Flutterwave::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Model::unguard();
    }
}
